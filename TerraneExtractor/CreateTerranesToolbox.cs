using System;
using System.IO;
using System.Drawing;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.ADF.BaseClasses;
using ESRI.ArcGIS.ADF.CATIDs;
using ESRI.ArcGIS.Framework;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Geoprocessing;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.GeoprocessingUI;

namespace TerraneExtractor
{
    /// <summary>
    /// Summary description for CreateTerranesToolbox.
    /// </summary>
    [Guid("6f21b265-d899-4e3f-bea9-d4c427d564cd")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("TerraneExtractor.CreateTerranesToolbox")]
    public sealed class CreateTerranesToolbox : BaseCommand
    {
        #region COM Registration Function(s)
        [ComRegisterFunction()]
        [ComVisible(false)]
        static void RegisterFunction(Type registerType)
        {
            // Required for ArcGIS Component Category Registrar support
            ArcGISCategoryRegistration(registerType);

            //
            // TODO: Add any COM registration code here
            //
        }

        [ComUnregisterFunction()]
        [ComVisible(false)]
        static void UnregisterFunction(Type registerType)
        {
            // Required for ArcGIS Component Category Registrar support
            ArcGISCategoryUnregistration(registerType);

            //
            // TODO: Add any COM unregistration code here
            //
        }

        #region ArcGIS Component Category Registrar generated code
        /// <summary>
        /// Required method for ArcGIS Component Category registration -
        /// Do not modify the contents of this method with the code editor.
        /// </summary>
        private static void ArcGISCategoryRegistration(Type registerType)
        {
            string regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Register(regKey);

        }
        /// <summary>
        /// Required method for ArcGIS Component Category unregistration -
        /// Do not modify the contents of this method with the code editor.
        /// </summary>
        private static void ArcGISCategoryUnregistration(Type registerType)
        {
            string regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Unregister(regKey);

        }

        #endregion
        #endregion

        private IApplication m_application;
        public CreateTerranesToolbox()
        {
            //
            // TODO: Define values for the public properties
            //
            base.m_category = "Frogtech"; //localizable text
            base.m_caption = "Create Terranes Toolbox";  //localizable text
            base.m_message = "Create Terranes Toolbox";  //localizable text 
            base.m_toolTip = "Create Terranes Toolbox";  //localizable text 
            base.m_name = "Create Terranes Toolbox";   //unique id, non-localizable (e.g. "MyCategory_ArcMapCommand")

            try
            {
                //
                // TODO: change bitmap name if necessary
                //
                string bitmapResourceName = GetType().Name + ".bmp";
                base.m_bitmap = new Bitmap(GetType(), bitmapResourceName);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message, "Invalid Bitmap");
            }
        }

        #region Overridden Class Methods

        /// <summary>
        /// Occurs when this command is created
        /// </summary>
        /// <param name="hook">Instance of the application</param>
        public override void OnCreate(object hook)
        {
            if (hook == null)
                return;

            m_application = hook as IApplication;

            //Disable if it is not ArcMap
            if (hook is IMxApplication)
                base.m_enabled = true;
            else
                base.m_enabled = false;

            // TODO:  Add other initialization code
        }

        /// <summary>
        /// Occurs when this command is clicked
        /// </summary>
        public override void OnClick()
        {
            try
            {


                string ftToolPath; // = Environment.GetEnvironmentVariable("");
                if (Environment.GetEnvironmentVariable("ARCHOME_USER") != "")
                    ftToolPath = Environment.GetEnvironmentVariable("ARCHOME_USER") + "ArcToolbox\\Toolboxes";
                else
                    ftToolPath = Environment.GetEnvironmentVariable("AGSDESKTOPJAVA") + "ArcToolbox\\Toolboxes";
                IWorkspaceFactory pToolboxWorkspaceFactory = new ToolboxWorkspaceFactory();
                IToolboxWorkspace pToolboxWorkspace = (IToolboxWorkspace)pToolboxWorkspaceFactory.OpenFromFile(ftToolPath, 0);
                IGPFunctionFactory pGPFunctionFact = new TerranesToolBoxFactory();
                IGPTool pGPTool;
                IGPFunctionTool pGPFunctionTool;
                IGPToolbox pGPToolbox;

                if (File.Exists(ftToolPath + "TerranesToolbox.tbx"))
                    File.Delete(ftToolPath + "TerranesToolbox.tbx");


                pGPToolbox = pToolboxWorkspace.CreateToolbox("TerranesToolbox", "custom");

                IGPFunction pGPFunction;
        
                pGPFunction = pGPFunctionFact.GetFunction("Terranes");
                pGPTool = pGPToolbox.CreateTool(esriGPToolType.esriGPFunctionTool, "", "", "", "Terranes", null);

                pGPFunctionTool = (IGPFunctionTool)pGPTool;
                pGPFunctionTool.Function = pGPFunction;
                pGPTool.Store();

                UID pUID = new UID();
                pUID.Value = "esriGeoprocessingUI.ArcToolboxExtension";

                IArcToolboxExtension pATBExt = (IArcToolboxExtension)m_application.FindExtensionByCLSID(pUID);
                IArcToolbox pAtb = pATBExt.ArcToolbox;
                pAtb.AddToolbox(pGPToolbox);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString());
            }

        }

        #endregion
    }
}
