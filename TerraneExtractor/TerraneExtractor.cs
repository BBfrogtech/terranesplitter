using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.DataSourcesFile;

using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geoprocessing;
using ESRI.ArcGIS.DataSourcesRaster;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.DataSourcesGDB;
using ESRI.ArcGIS.Output;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Catalog;
using System.Linq;
using System.Drawing;

namespace TerraneExtractor
{
    [Guid("283ca4f6-c477-4660-a636-551e97a45cdc")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("TerraneExtractor.TerraneExtractor")]
    public class TerraneExtractor : ESRI.ArcGIS.Geoprocessing.IGPFunction
    {

        const string mapImageFormat = "TIFF";
        public ESRI.ArcGIS.esriSystem.UID DialogCLSID
        {
            get { return null; }
        }

        public string DisplayName
        {
            get { return "Terrane Extractor"; }
        }
        IGPMessages toolMessages;
        IProgressor progressBar;
        IStepProgressor progressDisplay;
        IFeature currentSplitFeature, terraneFeature;
        IObjectCopy obCopy;
        IGeoProcessor gp;
        IMapDocument sourceMap;
        String targetPath;
        string terraneFCPath, outTerraneFCPath;
        bool exportedTerraneData;
        string templateFile;
        ITrackCancel trackProgress = null;
        int pageOffset = 4;

        struct tableContents
        {
            public string name;
            public string terraneType;
            public double basementAge;
            public double minimumAge;
            public double maximumAge;


        }
        IFeatureClass splitFC;
        string locationBoundsText;
        List<string> abstractList, nameList, referencesList, imagePathList;
        List<tableContents> tableDetailsForTerrane;
        string mxdName, currentMapLocationView;
        IFeatureLayer extractionGrid;
        string locationMXDPath;
        public void Execute(ESRI.ArcGIS.esriSystem.IArray paramvalues, ESRI.ArcGIS.esriSystem.ITrackCancel TrackCancel, ESRI.ArcGIS.Geoprocessing.IGPEnvironmentManager envMgr, ESRI.ArcGIS.Geodatabase.IGPMessages message)
        {
            string tempMap = "c:\\temp\\" + Guid.NewGuid().ToString() + ".mxd";
            thumbnailMap = new MapDocument();
            thumbnailMap.New(tempMap);
            //   locationMap.Open("C:\\projects\\Global Terranes Extract\\Frogtech Global Terranes - AOI Template.mxd");
            try
            {


                toolMessages = message;
                progressBar = TrackCancel.Progressor;
                progressDisplay = (IStepProgressor)progressBar;
                obCopy = new ObjectCopyClass();

                trackProgress = TrackCancel;

                //Load the toolboxes for the geoprocessing tools that will be used
                gp = new GeoProcessor();
                String toolboxPath = Environment.GetEnvironmentVariable("ARCHOME_USER") + "ArcToolbox\\Toolboxes\\Spatial Analyst Tools.tbx";
                gp.AddToolbox(toolboxPath);

                toolboxPath = Environment.GetEnvironmentVariable("ARCHOME_USER") + "ArcToolbox\\Toolboxes\\Conversion Tools.tbx";
                gp.AddToolbox(toolboxPath);

                toolboxPath = Environment.GetEnvironmentVariable("ARCHOME_USER") + "ArcToolbox\\Toolboxes\\Data Management Tools.tbx";
                gp.AddToolbox(toolboxPath);

                //            toolboxPath = @"C:\Program Files (x86)\FROGTECH python Scripts\FTPythonScripts_10p3v5.tbx";
                //          gp.AddToolbox(toolboxPath);
                gp.AddOutputsToMap = false;

                IGPValue pGPValue;
                IGPParameter pParam;
                IArray pArray = paramvalues;
                IGPUtilities gpUtil = new GPUtilities();
                //Get the input terranes MXD
                pParam = (IGPParameter)pArray.Element[0];
                pGPValue = gpUtil.UnpackGPValue(pParam);
                //IDEMapDocument mapDoc = (IDEMapDocument)pGPValue;
                string mxdInPath = pGPValue.GetAsText();

                //Get the AoI MXD
                pParam = (IGPParameter)pArray.Element[1];
                pGPValue = gpUtil.UnpackGPValue(pParam);

                locationMXDPath = pGPValue.GetAsText();



                //Get the feature class that will be used to split up the global terranes
                pParam = (IGPParameter)pArray.Element[2];
                pGPValue = gpUtil.UnpackGPValue(pParam);
                //IDEMapDocument mapDoc = (IDEMapDocument)pGPValue;
                string splitInFC = pGPValue.GetAsText();
                string splitFCFolder = splitInFC.Substring(0, splitInFC.LastIndexOf("\\"));
                string splitFCName = splitInFC.Substring(splitInFC.LastIndexOf("\\") + 1);
                IFeatureWorkspace splitWorkspace = createFCWorkspace(splitFCFolder);
                splitFC = splitWorkspace.OpenFeatureClass(splitFCName);



                //IEnumLayer enumLayer;
                // ILayer currentLayer = null, currentLayer2 = null;

                //  locMap.AddLayer(extractionGrid);

                //Get the terranes feature class that will be split apart - this will be in the terranes MXD but needs to be specified as other feature classes may be present
                pParam = (IGPParameter)pArray.Element[3];
                pGPValue = gpUtil.UnpackGPValue(pParam);
                //IDEMapDocument mapDoc = (IDEMapDocument)pGPValue;
                terraneFCPath = pGPValue.GetAsText();

                //Get the output directory
                pParam = (IGPParameter)pArray.Element[4];
                pGPValue = gpUtil.UnpackGPValue(pParam);
                //IDEMapDocument mapDoc = (IDEMapDocument)pGPValue;
                string outDirectory = pGPValue.GetAsText() + "\\" + splitFCName;

                //Get the PPT template file
                pParam = (IGPParameter)pArray.Element[5];
                pGPValue = gpUtil.UnpackGPValue(pParam);
                //IDEMapDocument mapDoc = (IDEMapDocument)pGPValue;
                templateFile = pGPValue.GetAsText();

                //Get the number of pages at the start of the PPT template which will be located before the terrane pages
                pParam = (IGPParameter)pArray.Element[6];
                pGPValue = gpUtil.UnpackGPValue(pParam);
                pageOffset = int.Parse(pGPValue.GetAsText());

                //Get the naming method. This will either be tile to indicate lat-long values will be used or the name of a field in the splitting feature class
                pParam = (IGPParameter)pArray.Element[7];
                pGPValue = gpUtil.UnpackGPValue(pParam);
                string namingMethod = pGPValue.GetAsText();

                //           // message.AddMessage(mxdInPath);
                //         // message.AddMessage(splitFC);
                //       // message.AddMessage(outDirectory);
                //     // message.AddMessage(templateFile);

                progressDisplay.MinRange = 0;
                progressDisplay.MaxRange = splitFC.FeatureCount(null);
                progressDisplay.StepValue = 1;
                progressDisplay.Show();

                //Load the terranes MXD
                sourceMap = new MapDocument();
                sourceMap.Open(mxdInPath);

                UID pId = new UID();
                pId.Value = "{40A9E885-5533-11d0-98BE-00805F7CED21}";

                //Get the first layer from the map
                //   IFeatureLayer terraneLayer = (IFeatureLayer) sourceMap.Map[0].Layer[0];
                //  IFeatureClass terraneFC = terraneLayer.FeatureClass;

                // ISpatialFilter terraneFilter = new SpatialFilter();

                //   terraneFilter.GeometryField = terraneFC.ShapeFieldName;
                //  terraneFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;
                // terraneFilter.SearchOrder = esriSearchOrder.esriSearchOrderSpatial;

                //     IQueryFilter qf = new QueryFilter();
                //    qf.WhereClause = "name = 'Extract 60W,42S - 32W,56S'";
                //Extract 60W,42S - 32W,56S
                IFeatureCursor splitCursor = splitFC.Search(null, true);

                //   IFeatureCursor terraneCursor;
                //   IGeometry resultGeom = null;
                //   ITopologicalOperator trimFeature;
                int folderNameIDX = splitFC.FindField("ET_ID");
                currentSplitFeature = splitCursor.NextFeature();



                // IWorkspace outWorkspace;
                IEnumLayer enumLayer, enumLayer2;
                ILayer currentLayer = null, currentLayer2 = null;

                IMap outMapMap = sourceMap.Map[0], outMapMap2;

                //Iterate through each layer in the terranes feature class in order to extract them
                enumLayer = outMapMap.get_Layers(null, false);


                int limitCounter = 0;
                int totalSplit = 0;
                while (currentSplitFeature != null)
                {
                    if (trackProgress.Continue() == false)
                        return;

                    exportedTerraneData = false;

                    //abstractList, nameList, referencesList, imagePathList
                    abstractList = new List<string>();
                    nameList = new List<string>();
                    referencesList = new List<string>();
                    imagePathList = new List<string>();
                    tableDetailsForTerrane = new List<tableContents>();
                    locationBoundsText = "";
                    //Work out what value to subtitute with when XX-YY is located in the map, filesnames and PPT template. These are in predetermined positions, it won't replace any new values added
                    //The value to replace with is either the lat/long bounds of the current splitting feature or a field value in the current splitting feature
                    if (namingMethod == "Tile")
                    {
                        int xVal = (int)Math.Round(currentSplitFeature.Extent.UpperLeft.X);
                        int yVal = (int)Math.Round(currentSplitFeature.Extent.UpperLeft.Y);
                        locationBoundsText += Math.Abs(xVal).ToString() + ((xVal < 0) ? "W" : "E");
                        locationBoundsText += ",";
                        locationBoundsText += Math.Abs(yVal).ToString() + ((yVal < 0) ? "S" : "N");

                        locationBoundsText += " - ";

                        xVal = (int)Math.Round(currentSplitFeature.Extent.LowerRight.X);
                        yVal = (int)Math.Round(currentSplitFeature.Extent.LowerRight.Y);
                        locationBoundsText += Math.Abs(xVal).ToString() + ((xVal < 0) ? "W" : "E");
                        locationBoundsText += ",";
                        locationBoundsText += Math.Abs(yVal).ToString() + ((yVal < 0) ? "S" : "N");
                    }
                    else
                    {
                        locationBoundsText = currentSplitFeature.get_Value(currentSplitFeature.Fields.FindField(namingMethod)).ToString();


                    }
                    //Determine the name of the output mxd
                    mxdName = mxdInPath.Substring(mxdInPath.LastIndexOf("\\") + 1);
                    mxdName = mxdName.Substring(0, mxdName.LastIndexOf("."));
                    mxdName = mxdName.Replace("XX - YY", locationBoundsText);
                    //Determine the directory to write the current output map and data to and create it
                    targetPath = outDirectory + "\\" + mxdName;// currentSplitFeature.get_Value(folderNameIDX).ToString();

                    Directory.CreateDirectory(targetPath);


                    //Create the TIFF file that shows where the current extraction is taking place
                    currentMapLocationView = exportMapLocation();


                    //.get_Map(0);
                    enumLayer.Reset();
                    currentLayer = enumLayer.Next();
                    // message.AddMessage(currentLayer.Name);
                    //Export each of the layers in the source MXD to the target folder. Note that any online data services will be skipped - the output MXD will refer to them in their original location
                    while (currentLayer != null)
                    {
                        //  if (currentLayer is IFeatureLayer && ((IDataset)(((IFeatureLayer)(currentLayer)).FeatureClass)).Workspace.Type == esriWorkspaceType.esriLocalDatabaseWorkspace)
                        exportLayers(currentLayer);
                        // message.AddMessage(currentLayer.Name);

                        currentLayer = enumLayer.Next();
                    }

                    //Work out the name of the target MXD for this extraction
                    string outputMXD = targetPath + "\\" + mxdName + ".mxd";

                    //                    File.Copy(mxdInPath, targetPath + "\\" + mxdInPath.Substring(mxdInPath.LastIndexOf("\\") + 1));
                    //Copy the source MXD to the target location, but use temp.mxd as the name since it will be resaved as a 10.1 compatible MXD
                    File.Copy(mxdInPath, targetPath + "\\temp.mxd");

                    IMapDocument copyMap = new MapDocument();
                    copyMap.Open(targetPath + "\\temp.mxd");

                    //Set the map document to be 10.1 compatible
                    ((IDocumentVersion)(copyMap)).DocumentVersion = esriArcGISVersion.esriArcGISVersion101;
                    //    outMap.SaveAs(outputMXD.Replace(".mxd", "_10_3.mxd"), true, false);
                    //Save the map out to the correct name 
                    copyMap.SaveAs(outputMXD, true, false);
                    copyMap.Close();
                    Marshal.FinalReleaseComObject(copyMap);
                    copyMap = null;
                    //Delete the temporary map
                    File.Delete(targetPath + "\\temp.mxd");
                    //  File.Copy(mxdInPath, outputMXD);

                    //Open the newly saved output MXD and update the title information
                    IMapDocument outMap = new MapDocument();
                    outMap.Open(outputMXD);
                    string mapTitle = ((IDocumentInfo2)(outMap)).DocumentTitle;

                    mapTitle = mapTitle.Replace("XX - YY", locationBoundsText);
                    ((IDocumentInfo2)(outMap)).DocumentTitle = mapTitle;


                    outMapMap2 = outMap.Map[0];

                    //CType(m_app.Document, IMxDocument).ActiveView.Extent

                    //Zoom the output map to the current splitting feature
                    IEnvelope zoomEnvelope = currentSplitFeature.ShapeCopy.Envelope;
                    if (zoomEnvelope.SpatialReference != outMapMap2.SpatialReference)
                    {
                        ((IGeometry)(zoomEnvelope)).Project(outMapMap2.SpatialReference);
                    }
                    //                    int convertedX, convertedY;



                    IActiveView activeView = outMap.ActiveView;

                    // Make sure ArcMap is in layout view  
                    /*
                    if (!(activeView is IPageLayout))
                    {
                        MessageBox.Show("Please switch to layout view.");
                        return;
                    }*/

                    // Look at the page layout as a container and loop through all the elements.  
                    /*
                    IGraphicsContainer graphicsContainer = activeView as IGraphicsContainer;
                    graphicsContainer.Reset();
                    IElement element = null;
                    if ((activeView is IPageLayout))
                    {
                        while ((element = graphicsContainer.Next()) != null)
                        {
                            // Only work with map frames  
                            if (element is IMapFrame)
                            {
                                // Get the map frame's map  
                                IMap map = (element as IMapFrame).Map;
                                // Only work with the focus map frame in layout  
                                if (map.Equals(outMapMap2))
                                {
                                    // Display the map frame's layout position/size  
                                    // which is in reference to lower left corner.  
                                    IGeometry geometry = element.Geometry;
                                    zoomEnvelope = geometry.Envelope;
                                    
                                }
                            }
                        }
                    }*/
                    /*
                    if (outMap.ActiveView is IPageLayout)
                    {
                        IEnvelope outZoomEnvelope = (IEnvelope)new Envelope();
                        ((IActiveView)(outMapMap2)).ScreenDisplay.DisplayTransformation.FromMapPoint(zoomEnvelope.UpperLeft, out convertedX, out convertedY);
                        outZoomEnvelope.UpperLeft = ((IActiveView)(outMap.PageLayout)).ScreenDisplay.DisplayTransformation.ToMapPoint(convertedX, convertedY);
                        

                      //  ((IActiveView)(outMapMap2)).ScreenDisplay.DisplayTransformation.FromMapPoint(zoomEnvelope.LowerLeft, out convertedX, out convertedY);
                       // outZoomEnvelope.LowerLeft = ((IActiveView)(outMapMap2)).ScreenDisplay.DisplayTransformation.ToMapPoint(convertedX, convertedY);

                        ((IActiveView)(outMapMap2)).ScreenDisplay.DisplayTransformation.FromMapPoint(zoomEnvelope.LowerRight, out convertedX, out convertedY);
                        outZoomEnvelope.LowerRight = ((IActiveView)(outMap.PageLayout)).ScreenDisplay.DisplayTransformation.ToMapPoint(convertedX, convertedY);

                       // ((IActiveView)(outMapMap2)).ScreenDisplay.DisplayTransformation.FromMapPoint(zoomEnvelope.UpperLeft, out convertedX, out convertedY);
                        //outZoomEnvelope.LowerLeft = ((IActiveView)(outMapMap2)).ScreenDisplay.DisplayTransformation.ToMapPoint(convertedX, convertedY);
                        zoomEnvelope = outZoomEnvelope;

                     
                    }*/

                    // else
                    ((IActiveView)(outMapMap2)).Extent = currentSplitFeature.Extent;
                    ((IActiveView)(outMapMap2)).Refresh();

                    // outMap.ActiveView.Extent = zoomEnvelope;
                    //outMap.ActiveView.Refresh();
                    //





                    enumLayer2 = outMapMap2.get_Layers(pId, true);

                    //IMeasuredGrid viewGrid;
                    //IGraphicsContainer graphicsContainer = outMap.PageLayout as IGraphicsContainer;
                    //IFrameElement frameElement = graphicsContainer.FindFrame(outMapMap2);
                    //IMapFrame mapFrame = frameElement as IMapFrame;
                    //IMapGrids mapGrids = null;
                    //mapGrids = mapFrame as IMapGrids;
                    //IMapGrid thisGrid =   mapGrids.get_MapGrid(0);
                    //if (thisGrid is IMeasuredGrid)
                    //{
                    //    viewGrid = (IMeasuredGrid)thisGrid;
                    //    Console.WriteLine(viewGrid.XIntervalSize);
                    //}
                    currentLayer2 = enumLayer2.Next();
                    int layerCounter = 1;
                    string outPath = "c:\\temp\\" + Guid.NewGuid().ToString();
                    Directory.CreateDirectory(outPath);

                    bool firstTerraneLayerFound = false;

                    //Iterate through each layer in the output map
                    while (currentLayer2 != null)
                    {
                        if (trackProgress.Continue() == false)
                            return;
                        IDataset checkLayer = (IDataset)currentLayer2;
                        //   string path = checkLayer.Workspace.PathName;


                        IFeatureClass currentFC = ((IFeatureLayer)(currentLayer2)).FeatureClass;
                        IFeatureLayer trimLayer = (IFeatureLayer)currentLayer2;

                        //Remove any symbols from the current layer that aren't being used anymore due to the trimming
                        TrimSymbology(ref trimLayer);

                        // message.AddMessage(((IDataset)(currentFC)).Workspace.PathName + "\\" + ((IDataset)(currentFC)).Name);
                        // message.AddMessage(outTerraneFCPath);
                        // message.AddMessage((currentFC != null).ToString());
                        // message.AddMessage((((IDataset)(currentFC)).Workspace.PathName + "\\" + ((IDataset)(currentFC)).Name == outTerraneFCPath).ToString());
                        // message.AddMessage((!firstTerraneLayerFound).ToString());
                        //If this is true, this is the topmost terrane layer

                        //If the current layer is the first terrane layer in the output then extra processing is required

                        if (currentFC != null && outTerraneFCPath != null && ((IDataset)(currentFC)).Workspace.PathName.ToLower() + "\\" + ((IDataset)(currentFC)).Name.ToLower() == outTerraneFCPath.ToLower() && !firstTerraneLayerFound)
                        {
                            int terraneNameIDX = currentFC.FindField("Name");
                            firstTerraneLayerFound = true;



                            string terranesWSPath = terraneFCPath.Substring(0, terraneFCPath.LastIndexOf("\\"));
                            IFeatureWorkspace refPathIn = createFCWorkspace(terranesWSPath);
                            ITable refTable = refPathIn.OpenTable("ft_global_terranes_references");
                            IFeatureWorkspace outRefWS = (IFeatureWorkspace)((IDataset)(currentFC)).Workspace;
                            IUID tableUID = new UID();
                            tableUID.Value = "esriGeodatabase.Object";
                            //Create the references table for the terranes
                            ITable refTableOut = outRefWS.CreateTable("ft_global_terranes_references", (IFields)((IClone)(refTable.Fields)).Clone(), (ESRI.ArcGIS.esriSystem.UID)tableUID, null, "");
                            // message.AddMessage(currentFC.AliasName);


                            //Get a list of all GUID values from the terranes that were extracted for this section of the map
                            List<string> guidValues = GetValuesForFeatureClassField(currentFC, "t_guid");
                            if (guidValues != null && guidValues.Count > 0)
                            {
                                // message.AddMessage(guidValues.Count.ToString());
                                string extractionString = "";
                                foreach (string addString in guidValues)
                                {
                                    if (extractionString == "")
                                        extractionString = "'" + addString + "'";
                                    else
                                        extractionString += ",'" + addString + "'";
                                }

                                IQueryFilter qfMatches = new QueryFilter();
                                qfMatches.WhereClause = "t_guid in (" + extractionString + ")";
                                //Now copy any references that exist for the terranes in the current output map
                                if (refTable.RowCount(qfMatches) > 0)
                                {
                                    ICursor readCursor = refTable.Search(qfMatches, true);
                                    ICursor writeCursor = refTableOut.Insert(true);
                                    IRow inRow, outRow;
                                    inRow = readCursor.NextRow();
                                    while (inRow != null)
                                    {
                                        outRow = (IRow)refTableOut.CreateRowBuffer();

                                        outRow.Value[1] = inRow.Value[1];
                                        outRow.Value[2] = inRow.Value[2];
                                        writeCursor.InsertRow(outRow);
                                        inRow = readCursor.NextRow();
                                    }

                                    writeCursor.Flush();
                                    writeCursor = null;
                                    outRow = null;
                                    writeCursor = null;

                                }
                            }


                            guidValues = null;
                            refTableOut = null;
                            tableUID = null;
                            outRefWS = null;

                            //todo:Need to test this
                            //      MakeRelationshipClass(currentFC, refTable);


                            //Create a relate in the output map between the references and their associated terranes
                            IMemoryRelationshipClassFactory pMemRelFact;
                            IRelationshipClass pRelClass;
                            pMemRelFact = new MemoryRelationshipClassFactoryClass();
                            pRelClass = pMemRelFact.Open("ft_global_terrane_references", (IObjectClass)currentFC, "T_GUID",
                              (IObjectClass)refTable, "T_GUID", "forward", "backward", esriRelCardinality.esriRelCardinalityOneToMany);

                            // Add it to the relates for the feature layer in Map
                            IRelationshipClassCollectionEdit pRelClassCollEdit;
                            pRelClassCollEdit = (IRelationshipClassCollectionEdit)currentLayer2;
                            pRelClassCollEdit.AddRelationshipClass(pRelClass);


                            refTable = null;
                            refPathIn = null;
                            //   IFeatureCursor searchTerranes = currentFC.Search(null, true);
                            //   IFeature currentTerrane = searchTerranes.NextFeature();
                            //  List<int> oidList = new List<int>();
                            /*
                            while (currentTerrane != null)
                            {
                                oidList.Add(currentTerrane.OID);
                                currentTerrane = searchTerranes.NextFeature();
                            }*/

                            //StreamWriter nameListOut = new StreamWriter("c:\\temp\\nameList.txt");
                            //nameListOut.WriteLine("Name list");
                            //foreach (string currentName in nameList)
                            //{
                            //    nameListOut.WriteLine(currentName);
                            //}
                            //nameListOut.Flush();
                            //nameListOut.Close();
                            //nameListOut = null;

                            //Hide any extra labels etc. on the output map ready for saving it out as a image
                            findAndHideExtraElements(outMap);
                            //Select each terrane in the output map in turn then output it to an image file
                            foreach (string currentName in nameList)
                            {
                                IQueryFilter pFilter = new QueryFilter();
                                pFilter.WhereClause = "name = '" + currentName.Replace("'", "''") + "'";
                                ((IFeatureSelection)(currentLayer2)).SelectFeatures(pFilter, esriSelectionResultEnum.esriSelectionResultNew, true);
                                outMap.ActiveView.Refresh();
                                int selected = ((IFeatureSelection)(currentLayer2)).SelectionSet.Count;

                                //                                   Dim pFilter As IQueryFilter = New QueryFilter
                                //      pFilter.WhereClause = selectedFeature.Table.OIDFieldName & " = " & selectedFeature.OID

                                //    CType(currentLayer, IFeatureSelection).SelectFeatures(pFilter, esriSelectionResultEnum.esriSelectionResultNew, True)
                                //   findAndRemoveLegendElement(outMap);
                                //  findLegendElement(outMap);
                                ExportActiveViewParameterized(1200, 1, mapImageFormat, outPath + "\\" + CleanFileName(currentName) + "." + mapImageFormat, true, outMap);

                                //    ExportActiveView(outMap.PageLayout, outPath + "\\" + currentName + "_02.png", "PNG", 96, outMap, zoomEnvelope);
                                //                                ExportActiveView(outMap.PageLayout, outPath + "\\" + currentName + ".png", "PNG", 96, outMap, currentSplitFeature.Extent);
                                //
                                //    if (legendElement != null)
                                //   {
                                //   graphicsContainer.AddElement(legendElement, 0);
                                //    IEnvelope legendEnv = (IEnvelope)new Envelope();
                                //  legendElement.QueryBounds(outMap.ActiveView.ScreenDisplay, legendEnv);
                                // ExportActiveView(outMap.PageLayout, outPath + "\\" + currentName + "_scale.png", "PNG", 96, outMap, legendEnv);
                                // }

                            }
                            ((IFeatureSelection)(currentLayer2)).Clear();

                            //The terrane layer was located, so quit the loop
                            // break;



                        }
                        currentLayer2 = enumLayer2.Next();
                    }

                    // IFeatureWorkspace outWS = (IFeatureWorkspace)checkTargetWorkSpace(((IDataset)(currentFC)).Workspace, false);
                    //terraneFCPath



                    //Export the map to a Powerpoint document
                    exportMapToPPT(outPath);
                    //Reactivate any elements that were removed prior to the map terranes being exported as images
                    restoreExtraElements(outMap);
                    //     enumLayer2.Reset();
                    //Rename any layers in the output map which have XX - YY in them to substitute in the name value being used
                    enumLayer2 = outMapMap2.get_Layers(null, false);
                    currentLayer2 = enumLayer2.Next();
                    while (currentLayer2 != null)
                    {
                        if (currentLayer2.Name.Contains("XX"))
                            currentLayer2.Name = currentLayer2.Name.Replace("XX - YY", locationBoundsText);
                        currentLayer2 = enumLayer2.Next();
                    }

                    //Resave the map with the current changes and set the compatibility to still be 10.1
                    ((IDocumentVersion)(outMap)).DocumentVersion = esriArcGISVersion.esriArcGISVersion101;
                    outMap.Save();
                    //   outMap.SaveAs(outputMXD.Replace(".mxd", "_10_3.mxd"), true, false);

                    // outMap.SaveAs(outputMXD, true, false);

                    Marshal.FinalReleaseComObject(outMap);
                    outMap = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    Directory.Delete(outPath, true);




                    //outTerraneFCPath

                    /*
                    outWorkspace = fetchOrCreateWorkspace(outDirectory + "\\" + currentSplitFeature.get_Value(folderNameIDX).ToString(), ((IDataset)(terraneLayer)).Workspace);

                    terraneFilter.Geometry = currentSplitFeature.Shape;
             

                    terraneCursor = terraneFC.Search(terraneFilter, true);

                    terraneFeature = terraneCursor.NextFeature();
                    while (terraneFeature != null)
                    {
                        trimFeature = (ITopologicalOperator)terraneFeature.ShapeCopy;

                        resultGeom = trimFeature.Intersect(currentSplitFeature.Shape, esriGeometryDimension.esriGeometry2Dimension);


                        terraneFeature = terraneCursor.NextFeature();
                    }

                    currentSplitFeature = splitCursor.NextFeature();*/

                    currentSplitFeature = splitCursor.NextFeature();
                    progressDisplay.Position = totalSplit++;
                    //                    progressDisplay.Step();

                    File.Delete(currentMapLocationView);
                    //  if (limitCounter++ == 1)
                    //     break;

                }

                //Copy over the terranes references table


            }
            catch (Exception ex)
            {
                message.AddError(0, ex.ToString());
            }
            finally
            {

                Marshal.FinalReleaseComObject(thumbnailMap);
                File.Delete(tempMap);
                /*
                                if (msPPOut != null)
                                {
                                    msPPPres = null;
                                    msPPOut.Quit();

                                    Marshal.FinalReleaseComObject(msPPOut);
                                    msPPOut = null;
                                    GC.Collect();
                                    GC.WaitForPendingFinalizers();
                                }*/
            }
        }

        /// <summary>
        /// Remove any characters from a file name to be written that aren't legal in a file name
        /// </summary>
        /// <param name="fileName">The file name to be cleaned</param>
        /// <returns></returns>
        private static string CleanFileName(string fileName)
        {
            return System.IO.Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }

        /// <summary>
        /// Remove any symbology from a feature layer if it is not being used in that layer. This onky works for unique value type symbology
        /// </summary>
        /// <param name="inLayer"></param>
        public void TrimSymbology(ref IFeatureLayer inLayer)
        {
            IGeoFeatureLayer inLayerGeo = (IGeoFeatureLayer)inLayer;
            IFeatureRenderer inRenderer = inLayerGeo.Renderer;
            if (inRenderer is IUniqueValueRenderer)
            {
                IUniqueValueRenderer uvRenderer = (IUniqueValueRenderer)inRenderer;
                IFeatureClass rendFC = inLayer.FeatureClass;

                if (rendFC != null)
                {
                    List<string> fcFieldValues = GetValuesForFeatureClassField(inLayer.FeatureClass, uvRenderer.Field[0]);
                    List<string> removeValues = new List<string>();
                    for (int counter = 0; counter < uvRenderer.ValueCount; counter++)
                    {
                        if (!fcFieldValues.Any(item => item == uvRenderer.Value[counter]))
                        {
                            removeValues.Add(uvRenderer.Value[counter]);
                        }
                    }
                    foreach (string removeValue in removeValues)
                        uvRenderer.RemoveValue(removeValue);

                }
                else
                {
                    toolMessages.AddError(0, "Could not find feature class for layer : " + inLayer.Name);
                }
            }
        }

        /// <summary>
        /// Get all non-null values inside of a feature class for a given field
        /// </summary>
        /// <param name="inFC"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public List<string> GetValuesForFeatureClassField(IFeatureClass inFC, string fieldName)
        {
            try
            {
                Dictionary<string, string> fcValues = new Dictionary<string, string>();
                IQueryFilter inFilter = new QueryFilter();
                inFilter.WhereClause = fieldName + " is not null";
                IFeatureCursor readFC = inFC.Search(inFilter, true);
                IFeature currentFeature;
                int fieldIDX = inFC.FindField(fieldName);
                currentFeature = readFC.NextFeature();
                while (currentFeature != null)
                {

                    if (!fcValues.ContainsKey(currentFeature.Value[fieldIDX].ToString()))
                    {
                        fcValues.Add(currentFeature.Value[fieldIDX].ToString(), currentFeature.Value[fieldIDX].ToString());
                    }


                    currentFeature = readFC.NextFeature();
                }
                Marshal.FinalReleaseComObject(readFC);
                Marshal.FinalReleaseComObject(inFilter);
                readFC = null;
                inFilter = null;
                return fcValues.Keys.ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString() + "  " + inFC.AliasName + " " + fieldName);
            }
            return null;

        }

        public IFeatureWorkspace createFCWorkspace(string pathName)
        {
            IWorkspaceFactory workspaceFactory;
            if (pathName.LastIndexOf(".mdb") > -1 || pathName.LastIndexOf(".MDB") > -1)
            //     workspaceFactory = new AccessWorkspaceFactory();
            {
                Type t = Type.GetTypeFromProgID("esriDataSourcesGDB.AccessWorkspaceFactory");
                System.Object obj = Activator.CreateInstance(t);

                workspaceFactory = (IWorkspaceFactory2)obj;
            }
            else if (pathName.ToLower().LastIndexOf(".sde") > -1)
                workspaceFactory = new SdeWorkspaceFactory();
            else if (pathName.ToLower().LastIndexOf(".gdb") > -1)
            //  workspaceFactory = new FileGDBWorkspaceFactory();
            {
                Type t = Type.GetTypeFromProgID("esriDataSourcesGDB.FileGDBWorkspaceFactory");
                System.Object obj = Activator.CreateInstance(t);
                workspaceFactory = (IWorkspaceFactory2)obj;
            }
            else
            {
                Type t = Type.GetTypeFromProgID("esriDataSourcesFile.ShapefileWorkspaceFactory");
                System.Object obj = Activator.CreateInstance(t);
                workspaceFactory = (IWorkspaceFactory2)obj; // new ShapefileWorkspaceFactory();

            }
            //    workspaceFactory = new ShapefileWorkspaceFactory();
            return (IFeatureWorkspace)workspaceFactory.OpenFromFile(pathName, 0);


        }
        /// <summary>
        /// Restore any non-essential elements to a map that were removed prior to the image being exported
        /// </summary>
        /// <param name="mapDoc"></param>
        public void restoreExtraElements(IMapDocument mapDoc)
        {
            string elementText;
            graphicsContainer = (IGraphicsContainer)mapDoc.PageLayout;
            if (removedElements == null)
                return;

            foreach (IElement restoreElement in removedElements)
            {
                if (restoreElement is ITextElement)
                {
                    elementText = ((ITextElement)(restoreElement)).Text;
                    elementText = elementText.Replace("XX - YY", locationBoundsText);
                    ((ITextElement)(restoreElement)).Text = elementText;
                }
                if (restoreElement is ISymbolCollectionElement)
                {
                    elementText = ((ISymbolCollectionElement)(restoreElement)).Text;
                }
                if (restoreElement is IMultiPartTextElement)
                {
                    IMultiPartTextElement elementParts = (IMultiPartTextElement)restoreElement;
                    for (int counter = 0; counter < elementParts.PartCount; counter++)
                    {
                        IElement currentElement = elementParts.QueryPart(counter);
                        if (currentElement is ITextElement)
                        {
                            elementText = ((ITextElement)(currentElement)).Text;
                            elementText = elementText.Replace("XX - YY", locationBoundsText);
                            //((ITextElement)(currentElement)).Text = elementText;
                            elementParts.ReplacePart(counter, elementText, currentElement.Geometry);
                        }
                    }

                    // elementText = ((ISymbolCollectionElement)(restoreElement)).Text;
                }
                graphicsContainer.AddElement((IElement)restoreElement, 0);

            }
        }

        List<IElement> removedElements;
        /// <summary>
        /// Remove any elements from the layout view that are there except from the map layers and the legend
        /// </summary>
        /// <param name="mapDoc"></param>
        public void findAndHideExtraElements(IMapDocument mapDoc)
        {
            IElement currentElement;
            graphicsContainer = (IGraphicsContainer)mapDoc.PageLayout;

            removedElements = new List<IElement>();

            graphicsContainer.Reset();
            //  string allElements = "";
            currentElement = graphicsContainer.Next();
            int elementCounter = 0;
            //  List<IElement> elementsToRemove = new List<IElement>();

            while (currentElement != null)
            {
                elementCounter++;

                string elementName = ((IElementProperties)(currentElement)).Name;
                //  allElements += elementName + " - ";
                if (elementName == "Layers" || elementName == "Legend") // !(currentElement is ESRI.ArcGIS.Carto.IMapSurroundFrame && ((ESRI.ArcGIS.Carto.IMapSurroundFrame)(currentElement)).MapSurround is ILegend))
                    //  if (currentElement is IPictureElement || currentElement is ITextElement)
                    // {

                    // currentElement.Deactivate();
                    //  storedLegendGeom = (IElement)((IClone)(currentElement)).Clone();
                    //   removedElements.Add((IElement)((IClone)(currentElement)).Clone());
                    Console.Write("Keep this one");
                else
                    removedElements.Add(currentElement);

                // }
                /*
                 if (elementName != "Layers" && elementName != "Legend") // !(currentElement is ESRI.ArcGIS.Carto.IMapSurroundFrame && ((ESRI.ArcGIS.Carto.IMapSurroundFrame)(currentElement)).MapSurround is ILegend))
                  //  if (currentElement is IPictureElement || currentElement is ITextElement)
                   // {
                        
                       // currentElement.Deactivate();
                      //  storedLegendGeom = (IElement)((IClone)(currentElement)).Clone();
                        removedElements.Add((IElement)((IClone)(currentElement)).Clone());

                        graphicsContainer.DeleteElement(currentElement);
                   // }
                 */
                currentElement = graphicsContainer.Next();
            }
           
            foreach (IElement deleteElement in removedElements)
                graphicsContainer.DeleteElement(deleteElement);

            //   graphicsContainer.DeleteAllElements();

            //    foreach (IElement restoreElement in removedElements)
            //   {
            //      graphicsContainer.AddElement((IElement)restoreElement, 0);
            // }
            // MessageBox.Show(allElements);
        }

        IElement legendElement = null;
        IGeometry storedLegendGeom = null;
        IGraphicsContainer graphicsContainer;
        //public void findAndRemoveLegendElement(IMapDocument mapDoc)
        //{
        //    IElement currentElement;
        //    graphicsContainer = (IGraphicsContainer)mapDoc.PageLayout;
        //    graphicsContainer.Reset();

        //    currentElement = graphicsContainer.Next();
        //    while (currentElement != null)
        //    {
        //        if (currentElement is ESRI.ArcGIS.Carto.IMapSurroundFrame)
        //            if (((ESRI.ArcGIS.Carto.IMapSurroundFrame)(currentElement)).MapSurround is ILegend)
        //            {
        //                legendElement = currentElement;
        //                storedLegendGeom = (IGeometry)((IClone)(legendElement.Geometry)).Clone();

        //            }
        //        currentElement = graphicsContainer.Next();
        //    }
        //    if (legendElement != null)
        //        graphicsContainer.DeleteElement(legendElement);



        //}

        //public void findLegendElement(IMapDocument mapDoc)
        //{
        //    IElement currentElement;
        //    graphicsContainer = (IGraphicsContainer)mapDoc.PageLayout;
        //    graphicsContainer.Reset();

        //    currentElement = graphicsContainer.Next();
        //    while (currentElement != null)
        //    {
        //        if (currentElement is ESRI.ArcGIS.Carto.IMapSurroundFrame)
        //            if (((ESRI.ArcGIS.Carto.IMapSurroundFrame)(currentElement)).MapSurround is ILegend)
        //            {
        //                legendElement = currentElement;
        //                storedLegendGeom = (IGeometry)((IClone)(legendElement.Geometry)).Clone();

        //            }
        //        currentElement = graphicsContainer.Next();
        //    }
        //    //   if (legendElement != null)
        //    //     graphicsContainer.DeleteElement(legendElement);



        //}

        /// <summary>
        /// Get the workspace to output the dataset into after it is trimmed 
        /// </summary>
        /// <param name="pathName"></param>
        /// <param name="isRaster"></param>
        /// <param name="sourceWorkspace"></param>
        /// <returns></returns>
        public IWorkspace fetchOrCreateWorkspace(String pathName, bool isRaster, IWorkspace sourceWorkspace)
        {
            IWorkspaceFactory2 workspaceFactory;
            bool isFolder = false;
            IWorkspace outWS = null;
            String folderName = pathName.Substring(0, pathName.LastIndexOf("\\"));
            String dbName = pathName.Substring(pathName.LastIndexOf("\\") + 1); ;
            try
            {
                //First get the workspace factory for the output location. Workspace factories are singletons so a reference to the existing one is created rather than one being created
                if (isRaster && (!pathName.ToLower().EndsWith(".mdb") && !pathName.ToLower().EndsWith(".gdb")))
                {
                    Type t = Type.GetTypeFromProgID("esriDataSourcesRaster.RasterWorkspaceFactory ");
                    System.Object obj = Activator.CreateInstance(t);

                    workspaceFactory = (IWorkspaceFactory2)obj;
                   // workspaceFactory = (IWorkspaceFactory2)new RasterWorkspaceFactoryClass();
                    if (!pathName.ToLower().EndsWith(".mdb") && !pathName.ToLower().EndsWith(".gdb"))
                    {
                        isFolder = true;
                    }
                }
                else if (pathName.LastIndexOf(".mdb") > -1 || pathName.LastIndexOf(".MDB") > -1)
                {
                    Type t = Type.GetTypeFromProgID("esriDataSourcesGDB.AccessWorkspaceFactory");
                    System.Object obj = Activator.CreateInstance(t);

                    workspaceFactory = (IWorkspaceFactory2)obj; // new AccessWorkspaceFactory();
                }
                //  else if (pathName.LastIndexOf(".sde") > -1 || pathName.LastIndexOf(".SDE") > -1)
                // {
                //    workspaceFactory = new SdeWorkspaceFactoryClass();
                // }
                else if (pathName.LastIndexOf(".gdb") > -1 || pathName.LastIndexOf(".GDB") > -1)
                {
                    Type t = Type.GetTypeFromProgID("esriDataSourcesGDB.FileGDBWorkspaceFactory");
                    System.Object obj = Activator.CreateInstance(t);
                    workspaceFactory = (IWorkspaceFactory2)obj;
                }
                else
                {
                    Type t = Type.GetTypeFromProgID("esriDataSourcesFile.ShapefileWorkspaceFactory");
                    System.Object obj = Activator.CreateInstance(t);
                    workspaceFactory = (IWorkspaceFactory2)obj; // new ShapefileWorkspaceFactory();
                    isFolder = true;
                }


                //If the output location is a geodatabase that exists, open it
                if (workspaceFactory.IsWorkspace(pathName) && ((pathName.EndsWith(".gdb") && Directory.Exists(pathName)) || (pathName.EndsWith(".mdb") && File.Exists(pathName))))
                    outWS = ((workspaceFactory.OpenFromFile(pathName, 0)));
                //If it doesn't exist, or isn't a geodatabase open it or create it if it doesn't exist
                if (outWS == null)
                {
                    if (isFolder)
                    {
                        if (!Directory.Exists(pathName))
                            Directory.CreateDirectory(pathName);
                        outWS = ((workspaceFactory.OpenFromFile(pathName, 0)));
                    }
                    else
                    {
                        if (!Directory.Exists(folderName))
                            Directory.CreateDirectory(folderName);
                        workspaceFactory.Create(folderName, dbName, null, 0);

                        outWS = ((workspaceFactory.OpenFromFile(pathName, 0)));
                        copyDomains(sourceWorkspace, outWS);
                    }
                }
            }
            catch (Exception ex)
            {
                toolMessages.AddError(0, ex.ToString() + "  " + pathName + "  " + folderName + "  " + dbName);
            }

            return outWS;

        }
        //Copy any domains from the source geodatabase to the output geodatabase
        public void copyDomains(IWorkspace inWS, IWorkspace outWS)
        {
            IWorkspaceDomains inWSD = (IWorkspaceDomains)inWS;
            IWorkspaceDomains outWSD = (IWorkspaceDomains)outWS;

            IEnumDomain enumWSDomains = inWSD.Domains;
            IDomain currentDomain;
            if (enumWSDomains != null)
            {
                enumWSDomains.Reset();
                currentDomain = enumWSDomains.Next();
                while (currentDomain != null)
                {
                    outWSD.AddDomain((IDomain)(((IClone)(currentDomain)).Clone()));
                    currentDomain = enumWSDomains.Next();
                }
            }
        }

        /// <summary>
        /// Export the layers from the source map so that the data is available for the output map
        /// </summary>
        /// <param name="currentLayer"></param>
        /// <returns></returns>
        private ILayer exportLayers(ILayer currentLayer)
        {
            ILayer outLayer = null, nextLayer;

            int pageCounter = 1;
            int nameIDX = 0, abstractIDX = 0, referencesIDX = 0;

            try
            {
                //For group layers ensure any sub layers are exported
                if (currentLayer is ICompositeLayer)
                {
                    outLayer = new GroupLayerClass();
                    outLayer.Name = currentLayer.Name;

                    outLayer.Visible = currentLayer.Visible;
                    ((ILayerEffects)(outLayer)).Transparency = ((ILayerEffects)(currentLayer)).Transparency;
                    ILayer addLayer;
                    for (int counter = 0; counter < ((ICompositeLayer)(currentLayer)).Count; counter++)
                    {

                        //   padString = "";
                        //  padString.PadLeft(depth * 2);

                        //     progressForm.addLayerLog(padString + ((ICompositeLayer)(currentLayer)).get_Layer(counter).Name);
                        if (((ICompositeLayer)(currentLayer)).get_Layer(counter).Valid)
                        {
                            nextLayer = ((ICompositeLayer)(currentLayer)).get_Layer(counter);
                            //  if (nextLayer is IFeatureLayer && ((IDataset)(((IFeatureLayer)(nextLayer)).FeatureClass)).Workspace.Type == esriWorkspaceType.esriLocalDatabaseWorkspace)
                            // {
                            addLayer = exportLayers(nextLayer);
                            if (addLayer != null)
                                ((IGroupLayer)(outLayer)).Add(addLayer);

                            //}
                        }
                    }

                }
                else
                {
                    //For dataset layers, which at this time are only going to be feature layers

                    ITopologicalOperator trimFeature;
                    IFeatureClass copyFC = null;
                    IFeatureWorkspace outWS;
                    //prgClipProgress.Increment(1);
                    //progressDisplay
                    //   lblStatus.Text = "Extracting : " + currentLayer.Name;
                    progressDisplay.Message = "Extracting : " + currentLayer.Name;
                    if (currentLayer is IFeatureLayer)
                    {

                        IFeatureClass currentFC = ((IFeatureLayer)(currentLayer)).FeatureClass;
                        //Only export feature data that comes from a local database and not an online service
                        if (((IDataset)(currentFC)).Workspace.Type == esriWorkspaceType.esriLocalDatabaseWorkspace)
                        {
                            outWS = (IFeatureWorkspace)checkTargetWorkSpace(((IDataset)(currentFC)).Workspace, false);

                            string outFCName = ((IDataset)(currentFC)).Name;

                            //Check to see if the feature class has already been copied over

                            if (((IWorkspace2)(outWS)).get_NameExists(esriDatasetType.esriDTFeatureClass, ((IDataset)(currentFC)).Name) || ((IWorkspace2)(outWS)).get_NameExists(esriDatasetType.esriDTRasterCatalog, ((IDataset)(currentFC)).Name))
                            {
                                copyFC = outWS.OpenFeatureClass(((IDataset)(currentFC)).Name);
                            }
                            else
                            {
                                //     if (((IDataset)(currentFC)).Workspace.PathName + "\\" + ((IDataset)(currentFC)).Name == terraneFCPath)
                                //       outFCName = "frogtech_global_terranes_extract_" + xString.Replace(" ", "") + "_" + yString.Replace(" ", "");
                                
                              //  toolMessages.AddMessage(exportedTerraneData.ToString());
                               // toolMessages.AddMessage(((IDataset)(currentFC)).Workspace.PathName + "\\" + ((IDataset)(currentFC)).Name.ToString());
                                //toolMessages.AddMessage(terraneFCPath.ToString());
                                // Check if this is the terrane layer and if it hasn't already been exported
                                if (exportedTerraneData == false && ((IDataset)(currentFC)).Workspace.PathName.ToLower() + "\\" + ((IDataset)(currentFC)).Name.ToLower() == terraneFCPath.ToLower())
                                {

                                    nameIDX = currentFC.FindField("Name");
                                    abstractIDX = currentFC.FindField("Abstract");
                                    referencesIDX = currentFC.FindField("References");
                                    outTerraneFCPath = ((IWorkspace)(outWS)).PathName + "\\" + ((IDataset)(currentFC)).Name;

                                }
                                IFields outFields = (IFields)((IClone)(currentFC.Fields)).Clone();
                                UID pUID = new UID();
                                pUID.Value = "esriGeodatabase.Feature";
                                IFeatureDataset targetDataset = null;

                                //Some of the feature classes will be stored in feature datasets inside of the geodatabase. If that is the case the create the dataset in the output location
                                if (currentFC.FeatureDataset != null)
                                {
                                    if (!((IWorkspace2)(outWS)).get_NameExists(esriDatasetType.esriDTFeatureDataset, currentFC.FeatureDataset.Name))
                                    {
                                        targetDataset = ((IFeatureWorkspace)(outWS)).CreateFeatureDataset(currentFC.FeatureDataset.Name, ((IGeoDataset)(currentFC.FeatureDataset)).SpatialReference);
                                    }
                                    //  outPath = outWS.PathName + "\\" + inFeatureClass.FeatureDataset.Name + "\\" + outName;

                                }
                                //Create the output feature class in the output location
                                if (targetDataset != null)
                                    copyFC = targetDataset.CreateFeatureClass(outFCName, outFields, pUID, null, esriFeatureType.esriFTSimple, currentFC.ShapeFieldName, null);
                                else
                                    copyFC = outWS.CreateFeatureClass(outFCName, outFields, pUID, null, esriFeatureType.esriFTSimple, currentFC.ShapeFieldName, null);

                                //                            if (targetDataset != null)
                                //                              copyFC = targetDataset.CreateFeatureClass(((IDataset)(currentFC)).Name, outFields, pUID, null, esriFeatureType.esriFTSimple, currentFC.ShapeFieldName, null);
                                //                        else
                                //                         copyFC = outWS.CreateFeatureClass(((IDataset)(currentFC)).Name, outFields, pUID, null, esriFeatureType.esriFTSimple, currentFC.ShapeFieldName, null);
                                IFeatureCursor matchingFeatures, exportedFeatures;
                                IFeature inFeature, outFeature;
                                /*
                                searchFilter = new SpatialFilter();

                                if (currentFC.ShapeType == esriGeometryType.esriGeometryPolygon || currentFC.ShapeType == esriGeometryType.esriGeometryPolyline)
                                {
                                    searchFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelCrosses;
                                }
                                else
                                {
                                    searchFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;
                                }
                                IGeometry searchGeom = inPolyFeature.ShapeCopy;
                                searchGeom.Project(((IGeoDataset)(currentFC)).SpatialReference);

                                searchFilter.Geometry = searchGeom;
                                searchFilter.GeometryField = currentFC.ShapeFieldName;

                                */

                                //Filter the input feature data so that only features that are touched by the splitting polygon are extracted
                                ISpatialFilter terraneFilter = new SpatialFilter();

                                terraneFilter.GeometryField = currentFC.ShapeFieldName;
                                terraneFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;
                                terraneFilter.SearchOrder = esriSearchOrder.esriSearchOrderSpatial;
                                terraneFilter.Geometry = currentSplitFeature.ShapeCopy;
                                matchingFeatures = currentFC.Search(terraneFilter, true);
                                //                                matchingFeatures = currentFC.Search(searchFilter, true);


                                exportedFeatures = copyFC.Insert(true);
                                inFeature = matchingFeatures.NextFeature();
                                //If there are no features in the polygon then don't include the feature class in the output

                                if (inFeature != null)
                                {
                                    trimFeature = (ITopologicalOperator)currentSplitFeature.ShapeCopy;
                                    if (((IGeometry)(trimFeature)).SpatialReference != inFeature.Shape.SpatialReference)
                                        ((IGeometry)(trimFeature)).Project(inFeature.Shape.SpatialReference);
                                    IGeometry trimGeom = (IGeometry)trimFeature;
                                    while (inFeature != null)
                                    {
                                        IGeometry currentGeom = (IGeometry)inFeature.Shape;

                                        if (currentGeom.IsEmpty == false)
                                        {
                                            outFeature = (IFeature)copyFC.CreateFeatureBuffer();
                                            // if (currentGeom.IsEmpty)
                                            //    Console.WriteLine("Pause");


                                            //Create the output geometry. If the input geometry is 3D and isn't a point then copy the entire geometry as is. Otherwise get the part which lies inside of the splitting geometry
                                            IGeometry resultGeom = null;
                                            if (((IZAware)(currentGeom)).ZAware && !(currentFC.ShapeType == esriGeometryType.esriGeometryPoint || currentFC.ShapeType == esriGeometryType.esriGeometryMultipoint))
                                                //  resultGeom = trimFeature.Intersect(currentGeom, esriGeometryDimension.esriGeometry25Dimension);
                                                if (((IProximityOperator)(trimFeature)).ReturnDistance(currentGeom) == 0)
                                                    resultGeom = (IGeometry)inFeature.ShapeCopy;
                                                else
                                                    resultGeom = null;
                                            //esriGeometry25Dimension
                                            else if (currentFC.ShapeType == esriGeometryType.esriGeometryPoint || currentFC.ShapeType == esriGeometryType.esriGeometryMultipoint)
                                                resultGeom = trimFeature.Intersect(currentGeom, esriGeometryDimension.esriGeometry0Dimension);
                                            else if (currentFC.ShapeType == esriGeometryType.esriGeometryPolyline)
                                                resultGeom = trimFeature.Intersect(currentGeom, esriGeometryDimension.esriGeometry1Dimension);
                                            else if (currentFC.ShapeType == esriGeometryType.esriGeometryPolygon)
                                                resultGeom = trimFeature.Intersect(currentGeom, esriGeometryDimension.esriGeometry2Dimension);
                                            else
                                                resultGeom = (IGeometry)inFeature.ShapeCopy;
                                            //The last one is likely only for multipatches which I won't try to split but will need to limit

                                            //For point geometries it doesn't appear that the intersect command will return any Z values that exist. So if they do intersect, use the originals.
                                            if (((IZAware)(currentGeom)).ZAware && (currentFC.ShapeType == esriGeometryType.esriGeometryPoint || currentFC.ShapeType == esriGeometryType.esriGeometryMultipoint))
                                            {
                                                if (!resultGeom.IsEmpty)
                                                    resultGeom = (IGeometry)inFeature.ShapeCopy;
                                            }
                                            //If there is a geometry from the intersection then insert it into the output feature class
                                            if (resultGeom != null && !resultGeom.IsEmpty)
                                            {
                                                outFeature.Shape = resultGeom;


                                                //Copy the field data from the input feature to the output feature
                                                for (int counter = 0; counter < inFeature.Fields.FieldCount; counter++)
                                                {
                                                    if (inFeature.Fields.get_Field(counter).Editable && inFeature.Fields.get_Field(counter).Type != esriFieldType.esriFieldTypeGeometry)
                                                    {
                                                        outFeature.set_Value(outFeature.Fields.FindField(inFeature.Fields.get_Field(counter).Name), inFeature.get_Value(counter));

                                                    }


                                                }
                                                exportedFeatures.InsertFeature((IFeatureBuffer)outFeature);
                                            }
                                            //If this is the terrane feature class, and it has not already been exported do an export for it.
                                            if (exportedTerraneData == false && ((IDataset)(currentFC)).Workspace.PathName.ToLower() + "\\" + ((IDataset)(currentFC)).Name.ToLower() == terraneFCPath.ToLower())
                                            {
                                                //Store the details about the current input feature for later export
                                                abstractList.Add(inFeature.get_Value(abstractIDX).ToString());
                                                nameList.Add(inFeature.get_Value(nameIDX).ToString());
                                                referencesList.Add(inFeature.get_Value(referencesIDX).ToString());

                                                tableContents currentTerraneContents = new tableContents();
                                               
                                                for (int counter = 0; counter < inFeature.Fields.FieldCount; counter++)
                                                {
                                                    if (!inFeature.get_Value(counter).Equals(DBNull.Value))
                                                    {
                                                        string checkName = inFeature.Fields.Field[counter].Name;
                                                        if (inFeature.Fields.Field[counter].Name.ToUpper() == "NAME")
                                                            currentTerraneContents.name = inFeature.get_Value(counter).ToString();
                                                        if (inFeature.Fields.Field[counter].Name.ToUpper() == "TYPE")
                                                            currentTerraneContents.terraneType = inFeature.get_Value(counter).ToString();
                                                        if (inFeature.Fields.Field[counter].Name.ToUpper() == "BASE_AGE")
                                                            currentTerraneContents.basementAge = (Double)inFeature.get_Value(counter);
                                                        if (inFeature.Fields.Field[counter].Name.ToUpper() == "MIN_AGE")
                                                            currentTerraneContents.minimumAge = (Double)inFeature.get_Value(counter);
                                                        if (inFeature.Fields.Field[counter].Name.ToUpper() == "MAX_AGE")
                                                            currentTerraneContents.maximumAge = (Double)inFeature.get_Value(counter);
                                                    }


                                                }
                                                tableDetailsForTerrane.Add(currentTerraneContents);
                                            }

                                        }

                                        inFeature = matchingFeatures.NextFeature();
                                        pageCounter += 1;
                                    }
                                    exportedFeatures.Flush();
                                    exportedFeatures = null;

                                }
                                inFeature = null;
                                outFeature = null;
                                matchingFeatures = null;
                                outFields = null;
                                pUID = null;

                                //Copy the metadata over
                                //   copyMetadata(((IDataset)(currentFC)).Workspace.PathName + "\\" + ((IDataset)(currentFC)).Name, ((IWorkspace)(outWS)).PathName + "\\" + ((IDataset)(copyFC)).Name);
                                //  copyMetadataBySync((IDataset)(currentFC), (IDataset)(copyFC));
                                IWorkspace checkWS = ((IDataset)(currentFC)).Workspace;
                                //Copy the metadata for the feature class across
                                if (checkWS.Type == esriWorkspaceType.esriLocalDatabaseWorkspace)
                                {
                                    copyMetadata(((IDataset)(currentFC)).Workspace.PathName + "\\" + ((IDataset)(currentFC)).Name, ((IWorkspace)(outWS)).PathName + "\\" + ((IDataset)(copyFC)).Name);

                                    //copyFC
                                    SetMetadataForExportedData((IDataset)currentFC, (IDataset)copyFC);

                                }
                                //    if (((IDataset)(currentFC)).Workspace is )
                            }
                        }



                        if (exportedTerraneData == false && ((IDataset)(currentFC)).Workspace.PathName.ToLower() + "\\" + ((IDataset)(currentFC)).Name.ToLower() == terraneFCPath.ToLower())
                        {

                            exportedTerraneData = true;
                        }

                        if (copyFC != null)
                        {
                            //  IFeatureLayer outLayer = (IFeatureLayer)obCopy.Copy(currentLayer);// new FeatureLayer();
                            outLayer = (ILayer)obCopy.Copy(currentLayer);
                            ((IFeatureLayer)(outLayer)).FeatureClass = copyFC;
                            // ((IGeoFeatureLayer)(outLayer)).Renderer = (IFeatureRenderer)obCopy.Copy(((IGeoFeatureLayer)(currentLayer)).Renderer);

                            // outMap.get_Map(0).AddLayer(outLayer);


                        }


                        copyFC = null;
                        outWS = null;
                        currentFC = null;



                    }
                    /*
                // If the layer is a raster layer process it, unless SEEBASE layers have been specified in which case don't do the image layer - it will be generated from the grid layer
                else if (currentLayer is IRasterLayer && (seebaseImagePath == "" || ((IRasterLayer)(currentLayer)).FilePath != seebaseImagePath))
                {
                    IWorkspace outWS = (IWorkspace)checkTargetWorkSpace(((IDataset)(currentLayer)).Workspace, true);
                    IWorkspace2 testWS = (IWorkspace2)checkTargetWorkSpace(((IDataset)(currentLayer)).Workspace, false);
                    IRasterLayer inRasterLayer = (IRasterLayer)currentLayer;
                    String rasterName = inRasterLayer.FilePath.Substring(inRasterLayer.FilePath.LastIndexOf("\\") + 1);


                    IRasterDataset inRasterDS = openRaster(((IDataset)(currentLayer)).Workspace, rasterName);
                    String targetRaster = "";

                    ISpatialReference rasterSR = ((IGeoDataset)(inRasterLayer)).SpatialReference;
                    IRasterDataset outRasterDS;




                    if (testWS.get_NameExists(esriDatasetType.esriDTRasterDataset, inRasterLayer.Name))
                    {
                        if (outWS is IRasterWorkspaceEx)
                            outRasterDS = ((IRasterWorkspaceEx)(outWS)).OpenRasterDataset(rasterName);
                        else
                            outRasterDS = ((IRasterWorkspace)(outWS)).OpenRasterDataset(rasterName);
                    }
                    else
                    {
                        IGeometry trimPoly = inPolyFeature.ShapeCopy;
                        if (((IWorkspace)(outWS)).PathName.EndsWith("\\"))
                            targetRaster = ((IWorkspace)(outWS)).PathName + rasterName;
                        else
                            targetRaster = ((IWorkspace)(outWS)).PathName + "\\" + rasterName;
                        //   gpParams.Add(targetRaster);
                        if (targetRaster.EndsWith(".ers") || targetRaster.EndsWith(".flt"))
                        {
                            targetRaster = targetRaster.Substring(0, targetRaster.LastIndexOf(".")) + ".img";
                            rasterName = targetRaster.Substring(targetRaster.LastIndexOf("\\") + 1);
                        }
                        if (((IRasterLayer)(currentLayer)).FilePath != seebaseGridPath)
                            seebaseGridOutPath = targetRaster;


                        outRasterDS = extractRasterUsingPolygonUsingSpatialAnalyst(trimPoly, currentLayer, inRasterDS, outWS, rasterName, targetRaster, true);
                        if (outRasterDS == null)
                            return null;
                           

                        copyMetadata(inRasterLayer.FilePath, targetRaster);


                    }

                    outLayer = new RasterLayerClass();
                    ((IRasterLayer)(outLayer)).CreateFromDataset((IRasterDataset)outRasterDS);
                    ((IRasterLayer)(outLayer)).Name = currentLayer.Name;
                    ((IRasterLayer)(outLayer)).Renderer = (IRasterRenderer)obCopy.Copy(inRasterLayer.Renderer);
                    ((IRasterLayer)(outLayer)).Visible = currentLayer.Visible;
                    ((ILayerGeneralProperties)(outLayer)).LayerDescription = ((ILayerGeneralProperties)(currentLayer)).LayerDescription;

                    // outMap.get_Map(0).AddLayer(outLayer);
                    Marshal.FinalReleaseComObject(outRasterDS);
                    outRasterDS = null;

                    //    if (cmbSEEBASEGrid.SelectedIndex > - 1 && currentLayer.Name == cmbSEEBASEGrid.SelectedItem.ToString() && seebaseImagePath != "")
                    //   {


                    // }
                }
                //Process the SEEBASE grid layer using the hill shade instead of clipping the image
                else if (currentLayer is IRasterLayer && (seebaseImagePath == "" || ((IRasterLayer)(currentLayer)).FilePath == seebaseImagePath))
                {
                       
                    IWorkspace outWS = (IWorkspace)checkTargetWorkSpace(((IDataset)(currentLayer)).Workspace, true);

                    String seebaseTargetRaster;
                    String seebaseRasterName = seebaseImagePath.Substring(seebaseImagePath.LastIndexOf("\\") + 1);
                    IRasterDataset outRasterDS;
                    IRasterLayer inRasterLayer = (IRasterLayer)currentLayer;

                    if (((IWorkspace)(outWS)).PathName.EndsWith("\\"))
                        seebaseTargetRaster = ((IWorkspace)(outWS)).PathName + seebaseRasterName;
                    else
                        seebaseTargetRaster = ((IWorkspace)(outWS)).PathName + "\\" + seebaseRasterName;
                    //   gpParams.Add(targetRaster);
                    if (seebaseTargetRaster.EndsWith(".ers") || seebaseTargetRaster.EndsWith(".flt"))
                    {
                        seebaseTargetRaster = seebaseTargetRaster.Substring(0, seebaseTargetRaster.LastIndexOf(".")) + ".img";
                        seebaseRasterName = seebaseTargetRaster.Substring(seebaseTargetRaster.LastIndexOf("\\") + 1);
                    }
                    //SEEBASE grid not already clipped, need to make temporary version
                    String tempFileName = "";
                    if (!File.Exists(seebaseGridOutPath))
                    {
                        //   IGeometry trimPoly = inPolyFeature.ShapeCopy;
                        //  String rasterName = inRasterLayer.FilePath.Substring(inRasterLayer.FilePath.LastIndexOf("\\") + 1);


                        // IRasterDataset inRasterDS = openRaster(((IDataset)(currentLayer)).Workspace, rasterName);
                        //   outRasterDS = 
                        // extractRasterUsingPolygonUsingSpatialAnalyst(trimPoly, currentLayer, inRasterDS, outWS, rasterName, "c:\\temp\\seebaseGridTemp.img", false);
                        //Clear the dataset object because it is only a temp file and will need to be processed using the hillshade algorithm
                        //    Marshal.FinalReleaseComObject(outRasterDS);
                        //   outRasterDS = null;
                        // Marshal.FinalReleaseComObject(inRasterDS);
                        //inRasterDS = null;


                        tempFileName = @"c:\temp\" + Guid.NewGuid().ToString() + ".img";
                        seebaseGridOutPath = "c:\\temp\\seebaseGridTemp.img";

                        extractRasterByMask(seebaseGridPath, seebaseGridOutPath);
                          

                    }


                    generateHillShade(seebaseGridOutPath, seebaseTargetRaster);
                    if (tempFileName != "")
                    {
                        if (File.Exists(seebaseGridOutPath))
                            File.Delete(seebaseGridOutPath);
                        if (File.Exists(seebaseGridOutPath + ".aux.xml"))
                            File.Delete(seebaseGridOutPath + ".aux.xml");
                        if (File.Exists(seebaseGridOutPath + ".ovr"))
                            File.Delete(seebaseGridOutPath + ".ovr");
                        if (File.Exists(seebaseGridOutPath + ".xml"))
                            File.Delete(seebaseGridOutPath + ".xml");

                    }

                    if (outWS is IRasterWorkspaceEx)
                        outRasterDS = ((IRasterWorkspaceEx)(outWS)).OpenRasterDataset(seebaseRasterName);
                    else
                        outRasterDS = ((IRasterWorkspace)(outWS)).OpenRasterDataset(seebaseRasterName);

                    copyMetadata(seebaseImagePath, seebaseTargetRaster);

                    outLayer = new RasterLayerClass();
                    ((IRasterLayer)(outLayer)).CreateFromDataset((IRasterDataset)outRasterDS);
                    ((IRasterLayer)(outLayer)).Name = currentLayer.Name;
                    ((IRasterLayer)(outLayer)).Renderer = (IRasterRenderer)obCopy.Copy(inRasterLayer.Renderer);
                    ((IRasterLayer)(outLayer)).Visible = currentLayer.Visible;
                    ((ILayerGeneralProperties)(outLayer)).LayerDescription = ((ILayerGeneralProperties)(currentLayer)).LayerDescription;

                    // outMap.get_Map(0).AddLayer(outLayer);
                    Marshal.FinalReleaseComObject(outRasterDS);
                    outRasterDS = null;
                }
                else if (currentLayer is IRasterLayer && (seebaseImagePath != "" || ((IRasterLayer)(currentLayer)).FilePath != seebaseImagePath))
                {

                    IWorkspace outWS = (IWorkspace)checkTargetWorkSpace(((IDataset)(currentLayer)).Workspace, true);

                    String seebaseTargetRaster;
                    String seebaseRasterName = seebaseImagePath.Substring(seebaseImagePath.LastIndexOf("\\") + 1);
                    IRasterDataset outRasterDS;
                    IRasterLayer inRasterLayer = (IRasterLayer)currentLayer;

                    if (((IWorkspace)(outWS)).PathName.EndsWith("\\"))
                        seebaseTargetRaster = ((IWorkspace)(outWS)).PathName + seebaseRasterName;
                    else
                        seebaseTargetRaster = ((IWorkspace)(outWS)).PathName + "\\" + seebaseRasterName;
                    //   gpParams.Add(targetRaster);
                    if (seebaseTargetRaster.EndsWith(".ers") || seebaseTargetRaster.EndsWith(".flt"))
                    {
                        seebaseTargetRaster = seebaseTargetRaster.Substring(0, seebaseTargetRaster.LastIndexOf(".")) + ".img";
                        seebaseRasterName = seebaseTargetRaster.Substring(seebaseTargetRaster.LastIndexOf("\\") + 1);
                    }

                    generateHillShade(seebaseGridPath, seebaseTargetRaster);

                    if (outWS is IRasterWorkspaceEx)
                        outRasterDS = ((IRasterWorkspaceEx)(outWS)).OpenRasterDataset(seebaseRasterName);
                    else
                        outRasterDS = ((IRasterWorkspace)(outWS)).OpenRasterDataset(seebaseRasterName);

                    copyMetadata(seebaseImagePath, seebaseTargetRaster);
                    //  ILayer outLayer;

                    outLayer = new RasterLayerClass();
                    ((IRasterLayer)(outLayer)).CreateFromDataset((IRasterDataset)outRasterDS);
                    ((IRasterLayer)(outLayer)).Name = currentLayer.Name;
                    ((IRasterLayer)(outLayer)).Renderer = (IRasterRenderer)obCopy.Copy(inRasterLayer.Renderer);
                    ((IRasterLayer)(outLayer)).Visible = currentLayer.Visible;
                    ((ILayerGeneralProperties)(outLayer)).LayerDescription = ((ILayerGeneralProperties)(currentLayer)).LayerDescription;

                    // outMap.get_Map(0).AddLayer(outLayer);
                    Marshal.FinalReleaseComObject(outRasterDS);
                    outRasterDS = null;

                }*/
                }
            }
            catch (Exception ex)
            {
                if (currentLayer != null)
                    MessageBox.Show(ex.ToString() + "  " + currentLayer.Name);
                else
                    MessageBox.Show(ex.ToString());
            }
            return outLayer;
        }
        /// <summary>
        /// Copy the metadata between datasets using the import metadata geoprocessing tool
        /// </summary>
        /// <param name="inData"></param>
        /// <param name="outData"></param>
        private void copyMetadata(string inData, string outData)
        {

            IVariantArray gpParams = new VarArray();
            gpParams.Add(inData);
            gpParams.Add("FROM_ARCGIS");
            gpParams.Add(outData);
            //gpParams.Add(true);
            gp.Execute("ImportMetadata", gpParams, null);

        }

        private void MakeRelationshipClass(IFeatureClass inFC, ITable inTable)
        {
            IVariantArray gpParams = new VarArray();
            gpParams.Add(inFC);
            gpParams.Add(inTable);
            gpParams.Add(((IDataset)(inFC)).Workspace.PathName + "\\References");
            gpParams.Add("ft_global_terranes_references");
            gpParams.Add("ft_global_terranes");
            gpParams.Add("T_GUID");
            gpParams.Add("T_GUID");
            //gpParams.Add(true);
            gp.Execute("CreateRelationshipClass", gpParams, null);
        }

        /// <summary>
        /// Get a template slide from the PPT document by name
        /// </summary>
        /// <param name="slideName"></param>
        /// <param name="msPPSlides"></param>
        /// <returns></returns>
        private Microsoft.Office.Interop.PowerPoint.CustomLayout GetSlide(string slideName, Microsoft.Office.Interop.PowerPoint.CustomLayouts msPPSlides)
        {
            for (int counter = 1; counter <= msPPSlides.Count; counter++)
            {
                if (msPPSlides[counter].Name == slideName)
                    return msPPSlides[counter];
            }
            MessageBox.Show("Could not find template slide named: " + slideName);
            return null;
        }
        /// <summary>
        /// Export the data for the current output map to a Powerpoint document
        /// </summary>
        /// <param name="imagePath"></param>
        private void exportMapToPPT(string imagePath)
        {
            Microsoft.Office.Interop.PowerPoint.Application msPPOut = null;
            Microsoft.Office.Interop.PowerPoint.Presentations msPPPres = null;
            Microsoft.Office.Interop.PowerPoint.Presentation msPPCurPres = null;
            Microsoft.Office.Interop.PowerPoint.Slides msPPSlides = null;

            Microsoft.Office.Interop.PowerPoint.Presentation templatePres = null;
            Microsoft.Office.Interop.PowerPoint.Slide newSlide = null;
            Microsoft.Office.Interop.PowerPoint.Shape namePlaceHolder, mapPlaceholder, mapPlaceholder2, abstractPlaceholder = null, referencesPlaceholder = null, tablePlaceholder;
            Microsoft.Office.Interop.PowerPoint.CustomLayout newSlideTemplate;

            msPPOut = new Microsoft.Office.Interop.PowerPoint.Application();
            msPPOut.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;
            // string pptTemplateLocation= exportOptions.templateLocation; // '"D:\Frogtech Projects\Sketch_PowerPoint_Export\Draft Section Sketch loop export.pptx"

            msPPPres = msPPOut.Presentations;

            templatePres = msPPPres.Open(templateFile, Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoFalse);
            msPPCurPres = templatePres;
            msPPSlides = msPPCurPres.Slides;

            tableContents[] allTableContents = tableDetailsForTerrane.ToArray();

            //
            newSlide = msPPSlides[1];
            newSlide.Delete();
            newSlideTemplate = GetSlide("Header_Page", msPPCurPres.SlideMaster.CustomLayouts);
            msPPSlides.AddSlide(1, newSlideTemplate); //[5]);
            //            Microsoft.Office.Interop.PowerPoint.CustomLayout slideTemplate = msPPCurPres.SlideMaster.CustomLayouts[5];

            newSlide = msPPSlides[1];


            //BasementTerranesLocation

            Microsoft.Office.Interop.PowerPoint.Shapes slideShapes = newSlide.Shapes;
            //Set the location name for this particular map in the output PPT document
            for (int counter2 = 1; counter2 <= slideShapes.Placeholders.Count; counter2++)
            {
                slideShapes.Placeholders[counter2].Name = newSlideTemplate.Shapes.Placeholders[counter2].Name;
                if (slideShapes.Placeholders[counter2].Name == "BasementTerranesLocation")
                {

                    slideShapes[counter2].TextFrame.TextRange.Text = "Extract " + locationBoundsText;
                    //"XX - YY", locationBoundsText
                }
            }
            //Replace the table of contents slide in the PPT document with the template version
            newSlide = msPPSlides[4];
            newSlide.Delete();

            newSlideTemplate = GetSlide("Table_Of_Contents", msPPCurPres.SlideMaster.CustomLayouts);

            msPPSlides.AddSlide(4, newSlideTemplate); // msPPCurPres.SlideMaster.CustomLayouts[6]);



            newSlide = msPPSlides[4];
            //Header_Page
            //Table_Of_Contents
            //Title and Content
            //TOCMap
            //Update the TOC slide using the correct values based on the number of terranes exported
            slideShapes = newSlide.Shapes;
            for (int counter2 = 1; counter2 <= slideShapes.Placeholders.Count; counter2++)
            {
                slideShapes.Placeholders[counter2].Name = newSlideTemplate.Shapes.Placeholders[counter2].Name;
                if (slideShapes.Placeholders[counter2].Name == "TOCPlaceholder")
                {
                    int productGuidePage = nameList.Count + pageOffset + 1;
                    string tocText = "Included Basement Terranes" + (char)(9) + "5" + Environment.NewLine + "User Guide                 " + (char)(9) + (char)(9) + productGuidePage.ToString();
                    //                    string tocText = "License" + (char)(9) + (char)(9) + (char)(9) + "2" + Environment.NewLine + "Basement Terranes" + (char)(9) + (char)(9) + "4" + Environment.NewLine + "Product Guide" + (char)(9) + (char)(9) + (char)(9) + productGuidePage.ToString();
                    //                    string tocText = "License" + (char)(9) + (char)(9) + (char)(9) + (char)(9) + (char)(9) + "2" + Environment.NewLine + "Basement Terranes" + (char)(9) + (char)(9) + (char)(9) + (char)(9) + "4" + Environment.NewLine + "Basement Terranes" + (char)(9) + (char)(9) + (char)(9) + (char)(9) + productGuidePage.ToString();
                    slideShapes[counter2].TextFrame.TextRange.Text = tocText;
                    //                  string shapeText =  slideShapes[counter2].TextFrame.TextRange.Text;
                    //                
                    //              shapeText = shapeText.Replace("XXX", productGuidePage.ToString());
                    //            slideShapes[counter2].TextFrame.TextRange.Text = shapeText;
                }
                if (slideShapes.Placeholders[counter2].Name == "BasementTerranesLocation")
                {

                    slideShapes.Placeholders[counter2].TextFrame.TextRange.Text = "Extract " + locationBoundsText;
                    //"XX - YY", locationBoundsText
                }
                if (slideShapes.Placeholders[counter2].Name == "TOCMap")
                {

                    mapPlaceholder2 = slideShapes.Placeholders[counter2];

                    //    float placeHolderWidth = mapPlaceholder.Width;
                    Microsoft.Office.Interop.PowerPoint.Shape mapImage = newSlide.Shapes.AddPicture(currentMapLocationView, Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoTrue, mapPlaceholder2.Left, mapPlaceholder2.Top, -1, -1);
                    //   mapImage.LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoTrue;
                    //mapImage.Fill.
                    //  mapImage.Width = placeHolderWidth;
                    //    mapImage.Select();
                    //   msPPOut.CommandBars.ExecuteMso("PictureFitCrop");
                    mapImage.Name = "TOCMap";
                    mapImage.AlternativeText = "";
                }
                //exportMapLocation
            }

            //TOCPlaceholder

            //   Microsoft.Office.Interop.PowerPoint.DocumentWindow acWindow = msPPOut.ActiveWindow;
            //       int winCount = msPPOut.Windows.Count;
            //      IEnumerator<Microsoft.Office.Interop.PowerPoint.DocumentWindow> winEnum = (IEnumerator<Microsoft.Office.Interop.PowerPoint.DocumentWindow>)msPPOut.Windows.GetEnumerator();
            //     Microsoft.Office.Interop.PowerPoint.DocumentWindows docWindows = msPPOut.Windows;
            //    winEnum.MoveNext();
            //  Microsoft.Office.Interop.PowerPoint.DocumentWindow checkWin = winEnum.Current;


            int counter;
            //   int pageOffset = 4;
            //   msPPOut.Windows[1].Activate();
            //msPPOut.Windows.GetEnumerator().Current
            newSlideTemplate = GetSlide("Title and Content", msPPCurPres.SlideMaster.CustomLayouts);


            //For each terrane in the exported map a separate slide will be made
            for (counter = 0; counter < nameList.Count; counter++)
            {
                msPPSlides.AddSlide(counter + pageOffset + 1, newSlideTemplate); // msPPCurPres.SlideMaster.CustomLayouts[1]);

                newSlide = msPPSlides[counter + pageOffset + 1];
                //    msPPOut.ActiveWindow.View.GotoSlide(counter + pageOffset);

                slideShapes = newSlide.Shapes;

                //ReferencesHeading
                //slideShapes[1].Name = "";
                /*
                for (int counter2 = 1; counter2 <= slideShapes.Count; counter2++)
                {
                    if (slideShapes[counter2].Name == "Map")
                    {

                        mapPlaceholder = slideShapes[counter2];
                        mapPlaceholder.Fill.UserPicture(imagePath + "\\" + nameList[counter] + "." + mapImageFormat);


                    }
                }*/
                //  for (int counter=1;counter <=  msPPCurPres.SlideMaster.CustomLayouts[1].Shapes.Placeholders.Count;counter++)

                //Set details for the current terrane in the current slide
                for (int counter2 = 1; counter2 <= slideShapes.Placeholders.Count; counter2++)
                {
                    // int testA = slideShapes.Placeholders.Count;
                    //int testB = msPPCurPres.SlideMaster.CustomLayouts[1].Shapes.Placeholders.Count;

                    //abstractList, nameList, referencesList, imagePathList

                    slideShapes.Placeholders[counter2].Name = newSlideTemplate.Shapes.Placeholders[counter2].Name;
                    //    Microsoft.Office.Interop.PowerPoint.Shape testA = msPPCurPres.SlideMaster.CustomLayouts[1].Shapes.Placeholders[counter2];
                    //   Microsoft.Office.Interop.PowerPoint.Shape testBA = slideShapes.Placeholders[counter2];


                    if (slideShapes.Placeholders[counter2].Name == "Name")
                    {
                        namePlaceHolder = slideShapes.Placeholders[counter2];
                        namePlaceHolder.TextFrame.TextRange.Text = nameList[counter];
                    }
                    else if (slideShapes.Placeholders[counter2].Name == "Abstract")
                    {
                        abstractPlaceholder = slideShapes.Placeholders[counter2];
                        abstractPlaceholder.TextFrame.TextRange.Text = abstractList[counter];
                        //abstractPlaceholder.Height
                    }
                    else if (slideShapes.Placeholders[counter2].Name == "References")
                    {
                        referencesPlaceholder = slideShapes.Placeholders[counter2];
                        referencesPlaceholder.TextFrame.TextRange.Text = referencesList[counter];
                    }

                    else if (slideShapes.Placeholders[counter2].Name == "Map")
                    {

                        mapPlaceholder = slideShapes.Placeholders[counter2];

                        //   float placeHolderWidth = mapPlaceholder.Width;
                        Microsoft.Office.Interop.PowerPoint.Shape mapImage = newSlide.Shapes.AddPicture(imagePath + "\\" + CleanFileName(nameList[counter]) + "." + mapImageFormat, Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoTrue, mapPlaceholder.Left, mapPlaceholder.Top, -1, -1);
                        //   mapImage.LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoTrue;
                        //mapImage.Fill.
                        //  mapImage.Width = placeHolderWidth;
                        //    mapImage.Select();
                        //   msPPOut.CommandBars.ExecuteMso("PictureFitCrop");
                        mapImage.Name = "Map";
                        mapImage.AlternativeText = "";
                    }
                    else if (slideShapes.Placeholders[counter2].Name == "Attributes")
                    {
                        tablePlaceholder = slideShapes.Placeholders[counter2];
                        Microsoft.Office.Interop.PowerPoint.Shape attributeTable = slideShapes.AddTable(2, 5, tablePlaceholder.Left, tablePlaceholder.Top, -1, -1);
                        for (int x = 1; x < 6; x++)
                            for (int y = 1; y < 3; y++)
                            {
                                attributeTable.Table.Cell(y, x).Shape.TextFrame.TextRange.Font.Size = 12;
                                attributeTable.Table.Cell(y, x).Shape.TextFrame.TextRange.Font.Name = "Calibri (Body)";

                            }

                        //Microsoft.Office.Interop.PowerPoint.Shape mapImage = newSlide.Shapes.AddPicture(imagePath + "\\" + nameList[counter] + ".png", Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoTrue, mapPlaceholder.Left, mapPlaceholder.Top, -1, -1);
                        /*      attributeTable.Table.Rows.Add();
                              attributeTable.Table.Columns.Add();
                              attributeTable.Table.Columns.Add();
                              attributeTable.Table.Columns.Add();
                              attributeTable.Table.Columns.Add();
                              attributeTable.Table.Columns.Add();
                              attributeTable.Table.Columns.Add();

                              attributeTable.Table.Rows.Add();
                              attributeTable.Table.Columns.Add();
                              attributeTable.Table.Columns.Add();
                              attributeTable.Table.Columns.Add();
                              attributeTable.Table.Columns.Add();
                              attributeTable.Table.Columns.Add();
                              attributeTable.Table.Columns.Add();*/

                        attributeTable.Table.Cell(1, 1).Shape.TextFrame.TextRange.Text = "Name";
                        attributeTable.Table.Cell(1, 2).Shape.TextFrame.TextRange.Text = "Type";
                        attributeTable.Table.Cell(1, 3).Shape.TextFrame.TextRange.Text = "Basement Age (Ma)";
                        attributeTable.Table.Cell(1, 4).Shape.TextFrame.TextRange.Text = "Minimum Age(Ma)";
                        attributeTable.Table.Cell(1, 5).Shape.TextFrame.TextRange.Text = "Maximum Age(Ma)";



                        attributeTable.Table.Cell(2, 1).Shape.TextFrame.TextRange.Text = tableDetailsForTerrane[counter].name;
                        attributeTable.Table.Cell(2, 2).Shape.TextFrame.TextRange.Text = tableDetailsForTerrane[counter].terraneType;
                        attributeTable.Table.Cell(2, 3).Shape.TextFrame.TextRange.Text = tableDetailsForTerrane[counter].basementAge.ToString();
                        attributeTable.Table.Cell(2, 4).Shape.TextFrame.TextRange.Text = tableDetailsForTerrane[counter].minimumAge.ToString();
                        attributeTable.Table.Cell(2, 5).Shape.TextFrame.TextRange.Text = tableDetailsForTerrane[counter].maximumAge.ToString();


                        attributeTable.Name = "Atributes";
                        attributeTable.AlternativeText = "";
                    }


                    //tableDetailsForTerrane

                    //nameIDX, abstractIDX, referencesIDX
                    //namePlaceHolder, mapPlaceholder, abstractPlaceholder, referencesPlaceholder, tablePlaceholder;
                }
                if (referencesPlaceholder != null && abstractPlaceholder != null)
                    referencesPlaceholder.Top = abstractPlaceholder.Top + abstractPlaceholder.Height + 40;
                //      foreach (Microsoft.Office.Interop.PowerPoint.Shape checkShape in slideShapes)
                //     {
                ///        Console.WriteLine(checkShape.Name);
                //  }



                for (int counter2 = 1; counter2 <= slideShapes.Count; counter2++)
                {
                    if (slideShapes[counter2].Name == "ReferencesHeading")
                    {
                        slideShapes[counter2].Top = abstractPlaceholder.Top + abstractPlaceholder.Height + 20;
                        slideShapes[counter2].TextFrame.TextRange.Text = "References";
                        break;
                    }

                }
                //slideShapes[1].Name = "";
            }
            string propList = "";
            //   Microsoft.Office.Core.DocumentProperty biCurrentProp;
            //    DocumentProperty testProp;

            /*           
            try
            {
                var biProps = msPPCurPres.BuiltInDocumentProperties;
                string checkType = biProps.GetType().ToString();
                ShowBuiltInDocumentProperties(biProps);

            }
                        catch (Exception ex)
            {

            }*/

            //  Microsoft.Office.Core.DocumentProperties bipropList = ( Microsoft.Office.Core.DocumentProperties)msPPCurPres.BuiltInDocumentProperties;
            //    bipropList[1];
            // for (int pCounter = 0; pCounter < bipropList.Count;pCounter++ )
            //{
            //   propList += bipropList[counter].Name + "  " + bipropList[counter].Value + Environment.NewLine;
            // }
            /*

                foreach (Microsoft.Office.Core.DocumentProperty property in msPPCurPres.BuiltInDocumentProperties)
                {
                    // Console.WriteLine(property);

                    propList += property.Name + "  " + property.Value + Environment.NewLine;
                }
            propList += "Custom" + Environment.NewLine;
            foreach ( Microsoft.Office.Core.DocumentProperty property in msPPCurPres.CustomDocumentProperties)
            {
                propList += property.Name + "  " + property.Value + Environment.NewLine;
               // Console.WriteLine(property);
            }
            */
            //First save the output PPT document
            msPPCurPres.SaveAs(targetPath + "\\" + mxdName + ".pptx", Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLPresentation, Microsoft.Office.Core.MsoTriState.msoTrue);
            //Then export the document to a PDF 
            msPPCurPres.ExportAsFixedFormat(targetPath + "\\" + mxdName + ".pdf", Microsoft.Office.Interop.PowerPoint.PpFixedFormatType.ppFixedFormatTypePDF, Microsoft.Office.Interop.PowerPoint.PpFixedFormatIntent.ppFixedFormatIntentPrint);

            foreach (Microsoft.Office.Interop.PowerPoint.Slide slide in msPPSlides)
                Marshal.FinalReleaseComObject(msPPSlides);
            Marshal.ReleaseComObject(msPPSlides);

            msPPCurPres.Close();



            //   GC.Collect();
            // GC.WaitForPendingFinalizers();

            Marshal.ReleaseComObject(templatePres);
            Marshal.FinalReleaseComObject(msPPCurPres);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            msPPOut.Quit();
            Marshal.FinalReleaseComObject(msPPOut);
            msPPOut = null;


            msPPCurPres = null;

            templatePres = null;

            msPPSlides = null;

        }

        //public static void ShowBuiltInDocumentProperties(object builtInProps)
        //{
        //    Console.WriteLine("Builtin Properties: START");
        //    object[] parameters;

        //    Type etype = builtInProps.GetType();
        //    foreach (int i in Enum.GetValues(etype))
        //    {
        //        object item = i.GetType().InvokeMember("Item",
        //  System.Reflection.BindingFlags.Default | System.Reflection.BindingFlags.GetProperty,
        //    null, i, null); // GetPropertyValue(builtInProps, "Item", i);

        //        object val = null;
        //        try
        //        {// val = GetPropertyValue(item, "Value");
        //        }
        //        catch { continue; }

        //        string name = Enum.GetName(etype, i).Substring(10);

        //        Console.WriteLine(String.Format("Item: {0} Name : {1} Value : {0} ", Convert.ToString(item), name,
        //        (null != val) ? Convert.ToString(val) : ""));
        //    }

        //    Console.WriteLine("Builtin Properties: END");
        //}

        //public static object GetPropertyValue(object src, object source,
        //string name) //, params object[] parameters)
        //{

        //    return source.GetType().InvokeMember(name,
        //  System.Reflection.BindingFlags.Default | System.Reflection.BindingFlags.GetProperty,
        //    null, source, null);
        //}



        private IRasterDataset extractRasterUsingPolygonUsingSpatialAnalyst(IGeometry trimPoly, ILayer currentLayer, IRasterDataset inRasterDS, IWorkspace outWS, string rasterName, string targetRaster, bool returnNewRaster)
        {
            IRasterDataset outRasterDS;

            IRasterLayer inRasterLayer = (IRasterLayer)currentLayer;
            //Check to see if the raster has any part of itself within the polygon
            //   string targetRaster;
            IPointCollection trimFeature;

            IEnvelope polyBound = trimPoly.Envelope;
            IEnvelope rasterBound = ((IRasterProps)(inRasterDS.CreateDefaultRaster())).Extent;
            //  IProximityOperator inPoly = inPolyFeature.ShapeCopy;
            trimFeature = (IPointCollection)currentSplitFeature.ShapeCopy;
            if (((IGeometry)(trimFeature)).SpatialReference != ((IGeoDataset)(inRasterLayer)).SpatialReference)
                ((IGeometry)(trimFeature)).Project(((IGeoDataset)(inRasterLayer)).SpatialReference);

            bool overlaps = false;
            // if (((polyBound.XMin >= rasterBound.XMin && polyBound.XMin <= rasterBound.XMax) || (polyBound.XMax >= rasterBound.XMin && polyBound.XMax <= rasterBound.XMax) || (polyBound.XMin <= rasterBound.XMin && polyBound.XMax >= rasterBound.XMax)) &&
            //    ((polyBound.XMin >= rasterBound.XMin && polyBound.XMin <= rasterBound.XMax) || (polyBound.XMax >= rasterBound.XMin && polyBound.XMax <= rasterBound.XMax) || (polyBound.XMin <= rasterBound.XMin && polyBound.XMax >= rasterBound.XMax)))
            if (((IProximityOperator)(trimFeature)).ReturnDistance(rasterBound) == 0)
                overlaps = true;
            //If there is no overlap, ignore this raster
            if (!overlaps)
                return null;


            extractRasterByMask(inRasterLayer.FilePath, targetRaster, trimFeature);

            if (returnNewRaster == false)
                return null;

            //IGeoDataset outDS = extractOp.Polygon((IGeoDataset)inRasterDS, (IPolygon)trimPoly, true);
            //   outRasterDS = outWS.OpenRasterDataset(inRasterLayer.FilePath.Substring(inRasterLayer.FilePath.LastIndexOf("\\") + 1));
            if (outWS is IRasterWorkspaceEx)
                outRasterDS = ((IRasterWorkspaceEx)(outWS)).OpenRasterDataset(rasterName);
            else
                outRasterDS = ((IRasterWorkspace)(outWS)).OpenRasterDataset(rasterName);
            return outRasterDS;
        }

        private void extractRasterByMask(string inPath, string targetRaster, IPointCollection trimFeature)
        {
            string pointList = "";
            for (int counter = 0; counter < trimFeature.PointCount; counter++)
            {
                if (counter > 0)
                    pointList += ";";
                pointList += "'" + trimFeature.Point[counter].X + " " + trimFeature.Point[counter].Y + "'";

            }
            IVariantArray gpParams = new VarArray();
            gpParams.Add(inPath);
            gpParams.Add(pointList);
            gpParams.Add(targetRaster);
            gpParams.Add("INSIDE");

            ////Executing: ExtractByPolygon "SEEBASE\SEEBASE (Grid)" '12039786.990116 2721573.102057';'13728482.35523 2721573.102057';'13728482.35523 820179.465765';'12039786.990116 820179.465765' c:\Temp\clipped_by_poly.img INSIDE

            gp.Execute("ExtractByPolygon", gpParams, null);
        }

        /*

        private void copyMetadata(string inData, string outData)
        {
            IVariantArray gpParams = new VarArray();
            gpParams.Add(inData);
            gpParams.Add("FROM_ARCGIS");
            gpParams.Add(outData);
            //gpParams.Add(true);
            gp.Execute("ImportMetadata", gpParams, null);

        }*/
        IMapDocument thumbnailMap = new MapDocument();


        /// <summary>
        /// Export the location map to a TIFF while displaying the location exported for the current map
        /// </summary>
        /// <returns></returns>
        private string exportMapLocation()
        {
            string mapImageLocation = targetPath + "\\LocationMap.tiff";
            //    IFeatureLayer extractionGrid = new FeatureLayer();
            //  extractionGrid.FeatureClass = splitFC;
            IMapDocument locationMap = new MapDocument();

            locationMap.Open(locationMXDPath);

            IEnumLayer enumLayerMap;
            //  ILayer currentLayerMap = null;
            IMap locMap = locationMap.Map[0];
            enumLayerMap = locMap.get_Layers(null, true);


            enumLayerMap.Reset();
            ILayer checkMapLayer = enumLayerMap.Next();
            //Find the terranes extraction layer
            while (checkMapLayer.Name != "ft_global_terranes_extract_grid")
            {
                checkMapLayer = enumLayerMap.Next();
                if (checkMapLayer == null)
                    return "";
            }
            //Subtitute in the feature class currently being used to extract terranes from the input map
            extractionGrid = (IFeatureLayer)checkMapLayer;
            extractionGrid.FeatureClass = splitFC;



            //IEnumLayer enumLayer;
            // ILayer currentLayer = null, currentLayer2 = null;

            //locMap.AddLayer(extractionGrid);
            //locMap.AddLayer(extractionGrid);
            //enumLayer = locMap.get_Layers(null, true);


            //enumLayer.Reset();
            //ILayer checkLayer = enumLayer.Next();
            //while (checkLayer.Name != "ft_global_terranes_extract_grid")
            //{
            //    checkLayer = enumLayer.Next();
            //    if (checkLayer == null)
            //        return null;
            //}
            // extractionGrid = (IFeatureLayer)checkLayer;


            IFeatureLayerDefinition layerDef = (IFeatureLayerDefinition)extractionGrid;


            //Set the definition query for the splitting feature layer so that only the current extract feature being shown will be visible
            layerDef.DefinitionExpression = "et_id = " + currentSplitFeature.get_Value(currentSplitFeature.Fields.FindField("et_id")).ToString();
            IEnvelope outExtent = (IEnvelope)new Envelope();
            outExtent.XMin = currentSplitFeature.Extent.XMin - 2 * Math.Abs(currentSplitFeature.Extent.XMax - currentSplitFeature.Extent.XMin);
            outExtent.XMax = currentSplitFeature.Extent.XMax + 2 * Math.Abs(currentSplitFeature.Extent.XMax - currentSplitFeature.Extent.XMin);
            outExtent.YMin = currentSplitFeature.Extent.YMin - 2 * Math.Abs(currentSplitFeature.Extent.YMax - currentSplitFeature.Extent.YMin);
            outExtent.YMax = currentSplitFeature.Extent.YMax + 2 * Math.Abs(currentSplitFeature.Extent.YMax - currentSplitFeature.Extent.YMin);


            ((IActiveView)(locationMap.Map[0])).Extent = outExtent;
            ((IActiveView)(locationMap.Map[0])).Refresh();
            //Export the map to an image file
            ExportActiveViewParameterized(400, 1, "TIFF", mapImageLocation, true, locationMap);
            locationMap.Close();
            Marshal.FinalReleaseComObject(locationMap);
            locationMap = null;

            return mapImageLocation;
        }


        /// <summary>
        /// Update the metadata for the exported feature class with the values specific for the export
        /// </summary>
        /// <param name="inData"></param>
        /// <param name="outData"></param>
        private void SetMetadataForExportedData(IDataset inData, IDataset outData)
        {
            //   string inPath = inData.Workspace.PathName + "\\" + inData.Name + ".xml";
            //  string outPath = inData.Workspace.PathName + "\\" + inData.Name + ".xml";
            //File.Copy(inPath, outPath);
            IGxObjectInternalName pGxObjectInternalName = (IGxObjectInternalName)new GxDataset();
            pGxObjectInternalName.InternalObjectName = inData.FullName;
            IMetadata inMeta = (IMetadata)pGxObjectInternalName;
            IXmlPropertySet2 pXpsSource = (IXmlPropertySet2)inMeta.Metadata;
            string titleText = pXpsSource.SimpleGetProperty("dataIdInfo/idCitation/resTitle");
            //   string titleText2 = pXpsSource.SimpleGetProperty("idinfo/citation/citeinfo/title");

            IGxObjectInternalName pGxObjectInternalNameOut = (IGxObjectInternalName)new GxDataset();
            pGxObjectInternalNameOut.InternalObjectName = outData.FullName;
            IMetadata pMD = (IMetadata)pGxObjectInternalNameOut;
            //    pMD.Synchronize(esriMetadataSyncAction.esriMSAAlways, 1);

            IXmlPropertySet2 pXps;// = (IXmlPropertySet2)pMD;// ((IClone)(pMD.Metadata)).Clone();

            IMetadata pMDOut = (IMetadata)pGxObjectInternalNameOut;
            pXps = (IXmlPropertySet2)pMDOut.Metadata;

            thumbnailMap.Map[0].ClearLayers();

            //Set the title text for the export data - this isn't copied over correctly by the geoprocessing metadata copy
            pXps.SetPropertyX("dataIdInfo/idCitation/resTitle", titleText, esriXmlPropertyType.esriXPTText, esriXmlSetPropertyAction.esriXSPAAddOrReplace, false);

            //  pXps.SetPropertyX("idinfo/citation/citeinfo/title", titleText, esriXmlPropertyType.esriXPTText, esriXmlSetPropertyAction.esriXSPAAddOrReplace, false);
            //pXps.SetPropertyX("idinfo/citation/citeinfo/ftname", titleText, esriXmlPropertyType.esriXPTText, esriXmlSetPropertyAction.esriXSPAAddOrReplace, false);
            //     IGxThumbnail outThumb = (IGxThumbnail)pGxObjectInternalNameOut;
            //    stdole.IPicture outPic = outThumb.Thumbnail;

            //Add a layer to the thumbnail map object with the dataset having it's metadata updated
            if (outData is IFeatureClass)
            {
                IFeatureLayer addLayer = new FeatureLayer();
                addLayer.FeatureClass = (IFeatureClass)outData;
                thumbnailMap.Map[0].AddLayer(addLayer);
            }
            else
            {
                IRasterLayer addLayer = new RasterLayer();
                addLayer.CreateFromDataset((IRasterDataset)outData);
                thumbnailMap.Map[0].AddLayer(addLayer);
            }
            //Export the map with the dataset out as an image
            ExportActiveViewParameterized(200, 1, "BMP", "c:\\temp\\thumbnail.bmp", true, thumbnailMap);

            //   Marshal.FinalReleaseComObject(thumbnailMap);
            //  thumbnailMap = null;
            //Load the image into memory
            Bitmap inImage = new Bitmap("c:\\temp\\thumbnail.bmp");


            Bitmap thumbImage = new Bitmap(inImage, 200, 133); // = inImage.Clone(new Rectangle(0, 0, 200, 133), inImage.PixelFormat); //.Clone(new Rectangle(0,0,200,160));
            inImage.Dispose();

            inImage = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();


            //  stdole.IPictureDisp outImage =  (stdole.IPictureDisp)stdole.GetIPictureDispFromPicture(inImage);


            //Convert the image to an older format which is what the metadata uses
            stdole.IPictureDisp outImage = (stdole.IPictureDisp)Microsoft.VisualBasic.Compatibility.VB6.Support.ImageToIPicture(thumbImage);
            //                Byte[] gifData = File.ReadAllBytes("c:\\temp\\thumbnail.bmp");
            //              string gifCode = Convert.ToBase64String(gifData);
            //   thumbImage.Dispose();
            //  thumbImage = null;

            //Set the image as the thumbnail for the metadata
            if (outImage != null)
            {
                pXps.SetPropertyX("Binary/Thumbnail/Data", outImage, esriXmlPropertyType.esriXPTPicture, esriXmlSetPropertyAction.esriXSPAAddOrReplace, false);
            }



            outImage = null;


            thumbImage.Dispose();
            thumbImage = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();

            File.Delete("c:\\temp\\thumbnail.bmp");
            //   File.Delete(tempMap);

            //   pXps.SaveAsFile("", "", true, "c:\\temp\\" + outData.Name + ".xml");

            pMDOut.Metadata = (IPropertySet)pXps;
            //    pMDOut.Synchronize(esriMetadataSyncAction.esriMSAAlways, 1);

        }



        //private void copyMetadataBySync(IDataset inData, IDataset outData)
        //{

        //    IGxObjectInternalName pGxObjectInternalName = (IGxObjectInternalName)new GxDataset();
        //    pGxObjectInternalName.InternalObjectName = inData.FullName;

        //    IGxObjectInternalName pGxObjectInternalNameOut = (IGxObjectInternalName)new GxDataset();
        //    pGxObjectInternalNameOut.InternalObjectName = outData.FullName;

        //    IMetadata pMD = (IMetadata)pGxObjectInternalName;
        //    //    pMD.Synchronize(esriMetadataSyncAction.esriMSAAlways, 1);

        //    IXmlPropertySet2 pXps = (IXmlPropertySet2)((IClone)(pMD.Metadata)).Clone();

        //    IMetadata pMDOut = (IMetadata)pGxObjectInternalNameOut;
        //    pMDOut.Metadata = (IPropertySet)pXps;
        //    pMDOut.Synchronize(esriMetadataSyncAction.esriMSAAlways, 1);

        //    try
        //    {
        //        IMapDocument thumbnailMap = new MapDocument();
        //        string tempMap = "c:\\temp\\" + Guid.NewGuid().ToString() + ".mxd";
        //        thumbnailMap.New(tempMap);
        //        if (outData is IFeatureClass)
        //        {
        //            IFeatureLayer addLayer = new FeatureLayer();
        //            addLayer.FeatureClass = (IFeatureClass)outData;
        //            thumbnailMap.Map[0].AddLayer(addLayer);
        //        }
        //        else
        //        {
        //            IRasterLayer addLayer = new RasterLayer();
        //            addLayer.CreateFromDataset((IRasterDataset)outData);
        //            thumbnailMap.Map[0].AddLayer(addLayer);
        //        }
        //        ExportActiveViewParameterized(200, 1, "BMP", "c:\\temp\\thumbnail.bmp", true, thumbnailMap);

        //        Byte[] gifData = File.ReadAllBytes("c:\\temp\\thumbnail.bmp");
        //        string gifCode = Convert.ToBase64String(gifData);




        //        pXps.SetPropertyX("Binary/Thumbnail/Data", gifCode, esriXmlPropertyType.esriXPTImage, esriXmlSetPropertyAction.esriXSPAAddOrReplace, true);
        //        pXps.SetPropertyX("Binary/Thumbnail/Data", gifCode, esriXmlPropertyType.esriXPTText, esriXmlSetPropertyAction.esriXSPAAddOrReplace, true);

        //        if (outData is IFeatureClass)
        //        {
        //            pXps.SetAttribute("Binary/Thumbnail/Data", "EsriPropertyType", "Picture", esriXmlSetPropertyAction.esriXSPAAddOrReplace);
        //        }
        //        else
        //        {
        //            pXps.SetAttribute("Binary/Thumbnail/Data", "EsriPropertyType", "PictureX", esriXmlSetPropertyAction.esriXSPAAddOrReplace);
        //        }

        //        //     pMDOut.Metadata = (IPropertySet)pXps;
        //        pMDOut.Synchronize(esriMetadataSyncAction.esriMSAAlways, 1);

        //        Marshal.FinalReleaseComObject(thumbnailMap);
        //        //  File.Delete("c:\\temp\\thumbnail.bmp");
        //        File.Delete(tempMap);
        //    }
        //    catch (Exception ex)
        //    {
        //        StreamWriter sr = new StreamWriter("C:\\temp\\clipLog.txt", true);
        //        sr.WriteLine(ex.ToString() + "  " + outData.Name);
        //        sr.Flush();
        //        sr.Close();
        //        sr = null;
        //    }

        //}



        private IRasterDataset openRaster(IWorkspace rasterWS, String rasterName)
        {
            //  IWorkspaceFactory wsFact = new RasterWorkspaceFactory();
            // IRasterWorkspace rasterWS = (IRasterWorkspace)wsFact.OpenFromFile(rasterPath, 0);

            // IWorkspace inWS = (IWorkspace)checkTargetWorkSpace(((IDataset)(currentLayer)).Workspace, true);
            IRasterDataset rasterDataset;
            if (rasterWS is IRasterWorkspaceEx)
                rasterDataset = ((IRasterWorkspaceEx)(rasterWS)).OpenRasterDataset(rasterName);
            else
                rasterDataset = ((IRasterWorkspace)(rasterWS)).OpenRasterDataset(rasterName);

            //  IRasterDataset rasterDataset = rasterWS.OpenRasterDataset(rasterName);
            return rasterDataset;

        }


        private void clearObject(ref System.Object clearObject)
        {
            if (clearObject == null)
                return;
            if (System.Runtime.InteropServices.Marshal.IsComObject(clearObject))
            {
                Marshal.FinalReleaseComObject(clearObject);
            }
            clearObject = null;
        }
        /// <summary>
        /// Work out what workspace to save into based on the source location and the output location
        /// </summary>
        /// <param name="inWorkspace"></param>
        /// <param name="isRaster"></param>
        /// <returns></returns>
        private IWorkspace checkTargetWorkSpace(IWorkspace inWorkspace, bool isRaster)
        {
            //Need to work out if the source workspace is relative to the MXD
            IDocumentInfo2 mapDocInfo = (IDocumentInfo2)sourceMap;
            //    toolMessages.AddMessage("map path = " + mapDocInfo.Path);

            String mapPath = mapDocInfo.Path;
            mapPath = mapPath.Substring(0, mapPath.LastIndexOf("\\"));

            String inPath = inWorkspace.PathName;
            if (inPath.EndsWith("\\"))
                inPath = inPath.Substring(0, inPath.Length - 1);

            String relativePath = createRelativePath(inPath, mapPath);
            relativePath = relativePath.Substring(0, relativePath.LastIndexOf("\\"));
            String outWSPath = "";
            //   MessageBox.Show(mapPath);
            //If true the data is from a path which is relative to the MXD and not an absolute path which would be on a different drive.
            // String relativePath = "";
            String tPath = "";


            //toolMessages.AddMessage("in path = " + inPath);
            //toolMessages.AddMessage("map path = " + mapPath);
            //toolMessages.AddMessage("relative path = " + relativePath);
            //toolMessages.AddMessage("target path = " + targetPath);

            if (relativePath.StartsWith("."))
            {
                outWSPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(targetPath, relativePath));
                tPath = outWSPath.Substring(0, outWSPath.LastIndexOf("\\"));
                relativePath = outWSPath.Substring(outWSPath.LastIndexOf("\\") + 1);

            }
            else// if (inWorkspace.Type != esriWorkspaceType.esriFileSystemWorkspace)
            {

                outWSPath = System.IO.Path.Combine(targetPath, inPath.Replace(":", "drive"));
                //   MessageBox.Show(outWSPath);
                relativePath = outWSPath.Substring(outWSPath.LastIndexOf("\\") + 1);
                /*  String dbExtension = "gdb";
                  if (inWorkspace.PathName.ToLower().EndsWith(".mdb"))
                      dbExtension = "mdb";
                  relativePath = "nonRelativeData." + dbExtension;
                  outWSPath = System.IO.Path.Combine(targetPath, "nonRelativeData." + dbExtension);*/
                tPath = outWSPath.Substring(0, outWSPath.LastIndexOf("\\"));
            }
            //  else
            // {
            //    relativePath = "nonRelativeData";
            //  outWSPath = System.IO.Path.Combine(targetPath, "nonRelativeData");
            //tPath = targetPath;
            //}

            return fetchOrCreateWorkspace(outWSPath, isRaster, inWorkspace);


        }


        public String createRelativePath(String filePath, String mainPath)
        {
            String[] mainSegments = mainPath.ToLower().Split('\\');
            String[] fileSegments = filePath.ToLower().Split('\\');
            int counter, counter2;
            bool matches = true;

            String returnPath = "";
            if (mainSegments[0] != fileSegments[0])
            {
                return filePath + "\\";
            }
            if (mainSegments.Length <= fileSegments.Length)
            {
                for (counter = 0; counter < mainSegments.Length; counter++)
                {
                    if (mainSegments[counter] != fileSegments[counter])
                    {
                        matches = false;
                        break;
                    }

                }
            }
            else
            {
                matches = false;

                if (mainSegments[0] == fileSegments[0])
                {
                    for (counter = 0; counter < mainSegments.Length; counter++)
                    {
                        if (counter == fileSegments.Length || mainSegments[counter] != fileSegments[counter])
                        {

                            break;
                        }

                    }
                }
                else
                {
                    counter = mainSegments.Length - fileSegments.Length;
                }
            }

            if (matches && mainSegments.Length == fileSegments.Length)
            {
                return ".\\";
            }
            else if (matches)
            {
                returnPath = ".\\";
                for (counter2 = counter; counter2 < fileSegments.Length; counter2++)
                {
                    returnPath += fileSegments[counter2] + "\\";

                }
                return returnPath;
            }

            for (counter2 = counter; counter2 < mainSegments.Length; counter2++)
            {
                returnPath += "..\\";
            }
            for (counter2 = counter; counter2 < fileSegments.Length; counter2++)
            {
                returnPath += fileSegments[counter2] + "\\";
            }
            return returnPath;
        }




        public ESRI.ArcGIS.esriSystem.IName FullName
        {
            get
            {
                TerranesToolBoxFactory myFact = new TerranesToolBoxFactory();
                return (IName)myFact.GetFunctionName("");

            }
        }

        public object GetRenderer(ESRI.ArcGIS.Geoprocessing.IGPParameter pParam)
        {
            return null;
        }

        public int HelpContext
        {
            get { return 0; }
        }

        public string HelpFile
        {
            get { return ""; }
        }

        public bool IsLicensed()
        {
            return true;
        }

        public string MetadataFile
        {
            get { return "TerraneExtractor.xml"; }
        }

        public string Name
        {
            get { return "TerraneExtractor"; }
        }

        public ESRI.ArcGIS.esriSystem.IArray ParameterInfo
        {
            get
            {
                IArray parameters = new ESRI.ArcGIS.esriSystem.ArrayClass();
                IGPValue pValue;
                IGPParameterEdit pParamEdit;
                pParamEdit = (IGPParameterEdit)new GPParameter();
                pParamEdit.DataType = (IGPDataType)new DEMapDocumentType();

                pValue = (IGPValue)new DEMapDocument();
                pParamEdit.Value = pValue;

                pParamEdit.Direction = esriGPParameterDirection.esriGPParameterDirectionInput;
                pParamEdit.DisplayName = "Input Map";
                pParamEdit.Enabled = true;
                pParamEdit.Name = "input_map";
                pParamEdit.ParameterType = esriGPParameterType.esriGPParameterTypeRequired;
                parameters.Add(pParamEdit);


                pParamEdit = (IGPParameterEdit)new GPParameter();
                pParamEdit.DataType = (IGPDataType)new DEMapDocumentType();

                pValue = (IGPValue)new DEMapDocument();
                pParamEdit.Value = pValue;

                pParamEdit.Direction = esriGPParameterDirection.esriGPParameterDirectionInput;
                pParamEdit.DisplayName = "Location Map";
                pParamEdit.Enabled = true;
                pParamEdit.Name = "location_map";
                pParamEdit.ParameterType = esriGPParameterType.esriGPParameterTypeRequired;
                parameters.Add(pParamEdit);



                pParamEdit = (IGPParameterEdit)new GPParameter();
                pParamEdit.DataType = (IGPDataType)new DEFeatureClassType(); // GPFeatureLayerType();
                pValue = (IGPValue)new DEFeatureClass();
                pParamEdit.Value = pValue;

                pParamEdit.Direction = esriGPParameterDirection.esriGPParameterDirectionInput;
                pParamEdit.DisplayName = "Clipping Feature Class";
                pParamEdit.Enabled = true;
                pParamEdit.Name = "input_feature";
                pParamEdit.ParameterType = esriGPParameterType.esriGPParameterTypeRequired;
                parameters.Add(pParamEdit);

                pParamEdit = (IGPParameterEdit)new GPParameter();
                pParamEdit.DataType = (IGPDataType)new DEFeatureClassType(); // GPFeatureLayerType();
                pValue = (IGPValue)new DEFeatureClass();
                pParamEdit.Value = pValue;

                pParamEdit.Direction = esriGPParameterDirection.esriGPParameterDirectionInput;
                pParamEdit.DisplayName = "Terrane Feature Class";
                pParamEdit.Enabled = true;
                pParamEdit.Name = "input_terrane";
                pParamEdit.ParameterType = esriGPParameterType.esriGPParameterTypeRequired;
                parameters.Add(pParamEdit);

                pParamEdit = (IGPParameterEdit)new GPParameter();
                pParamEdit.DataType = (IGPDataType)new DEFolderType();
                pValue = (IGPValue)new DEFolder();
                pParamEdit.Value = pValue;

                pParamEdit.Direction = esriGPParameterDirection.esriGPParameterDirectionOutput;
                pParamEdit.DisplayName = "Output Directory";
                pParamEdit.Enabled = true;
                pParamEdit.Name = "output_dir";
                pParamEdit.ParameterType = esriGPParameterType.esriGPParameterTypeRequired;
                parameters.Add(pParamEdit);


                pParamEdit = (IGPParameterEdit)new GPParameter();
                pParamEdit.DataType = (IGPDataType)new DEFileType();

                pValue = (IGPValue)new DEFile();
                pParamEdit.Value = pValue;

                pParamEdit.Direction = esriGPParameterDirection.esriGPParameterDirectionInput;
                pParamEdit.DisplayName = "PowerPoint Template";
                pParamEdit.Enabled = true;
                pParamEdit.Name = "PPT_Template";
                pParamEdit.ParameterType = esriGPParameterType.esriGPParameterTypeRequired;
                parameters.Add(pParamEdit);

                pParamEdit = (IGPParameterEdit)new GPParameter();
                pParamEdit.DataType = (IGPDataType)new GPLongType();

                pValue = (IGPValue)new GPLong();
                pValue.SetAsText("5");
                pParamEdit.Value = pValue;

                pParamEdit.Direction = esriGPParameterDirection.esriGPParameterDirectionInput;
                pParamEdit.DisplayName = "Number of opening pages in Template";
                pParamEdit.Enabled = true;
                pParamEdit.Name = "PPT_Pages";
                pParamEdit.ParameterType = esriGPParameterType.esriGPParameterTypeRequired;
                parameters.Add(pParamEdit);


                pParamEdit = (IGPParameterEdit)new GPParameter();
                pParamEdit.DataType = new GPStringType();
                pValue = new GPString();
                pValue.SetAsText("Tile");
                pParamEdit.Value = pValue;
                pParamEdit.Direction = esriGPParameterDirection.esriGPParameterDirectionInput;
                pParamEdit.DisplayName = "Title Field";
                pParamEdit.Enabled = true;
                pParamEdit.Name = "TitleField";

                //IGPCodedValueDomain pDomain = new GPCodedValueDomainClass();
                //pDomain.AddStringCode("Title", "Title");
                //pDomain.AddStringCode("Tile", "Tile");

                //pParamEdit.Domain = (IGPDomain)pDomain;

                pParamEdit.ParameterType = esriGPParameterType.esriGPParameterTypeRequired;
                parameters.Add(pParamEdit);


                return parameters;
            }
        }

        public ESRI.ArcGIS.Geodatabase.IGPMessages Validate(ESRI.ArcGIS.esriSystem.IArray paramvalues, bool updateValues, ESRI.ArcGIS.Geoprocessing.IGPEnvironmentManager envMgr)
        {
            IGPMessages messages = (IGPMessages)new GPMessages();
            try
            {
                IGPValue pGPValue;
                IGPParameter pParam;
                IArray pArray = paramvalues;
                IGPUtilities gpUtil = new GPUtilities();
                IGPParameterEdit pParamEdit;
                pParam = (IGPParameter)pArray.Element[2];

                //Get the field names for the splitting feature class and add them to the domain for the title field so they will appear in a combo
                if (pParam != null)
                {
                    pGPValue = gpUtil.UnpackGPValue(pParam);
                    //IDEMapDocument mapDoc = (IDEMapDocument)pGPValue;
                    string splitInFC = pGPValue.GetAsText();
                    if (splitInFC != "")
                    {
                        string splitFCFolder = splitInFC.Substring(0, splitInFC.LastIndexOf("\\"));
                        string splitFCName = splitInFC.Substring(splitInFC.LastIndexOf("\\") + 1);
                        IFeatureWorkspace splitWorkspace = createFCWorkspace(splitFCFolder);
                        splitFC = splitWorkspace.OpenFeatureClass(splitFCName);
                        IGPCodedValueDomain nameDomain = new GPCodedValueDomain();
                        nameDomain.Empty();
                        IGPValue addCodeValue;

                        addCodeValue = new GPString();
                        addCodeValue.SetAsText("Tile");
                        nameDomain.AddCode(addCodeValue, "Tile");

                        for (int counter = 0; counter < splitFC.Fields.FieldCount; counter++)
                        {
                            if (splitFC.Fields.Field[counter].Type < esriFieldType.esriFieldTypeOID)
                            {
                                addCodeValue = new GPString();
                                addCodeValue.SetAsText(splitFC.Fields.Field[counter].Name);
                                nameDomain.AddCode(addCodeValue, splitFC.Fields.Field[counter].Name);
                            }
                        }

                        pParam = (IGPParameter)pArray.Element[7];
                        pParamEdit = (IGPParameterEdit)pParam;
                        pParamEdit.Domain = (IGPDomain)nameDomain;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }


            return messages;
        }

        private void ExportActiveView(IPageLayout pPageLayout, string outputFileName, string ExportType, int dpiValue, IMapDocument outDoc, IEnvelope zoomOutExtent)
        {
            //  IEnvelope zoomOutExtent = outDoc.ActiveView.Extent;
            // tagRECT exportRect = new tagRECT();
            //IPaper pPaper = 
            IExport docExport;

            if (ExportType == "PDF")
            {
                docExport = new ExportPDFClass();
            }
            else if (ExportType == "EPS")
            {
                docExport = new ExportPSClass();
            }
            else if (ExportType == "AI")
            {
                docExport = new ExportAIClass();
            }
            else if (ExportType == "BMP")
            {
                docExport = new ExportBMPClass();
            }
            else if (ExportType == "TIFF")
            {
                docExport = new ExportTIFFClass();
            }
            else if (ExportType == "SVG")
            {
                docExport = new ExportSVGClass();
            }
            else if (ExportType == "PNG")
            {
                docExport = new ExportPNGClass();
            }
            else if (ExportType == "GIF")
            {
                docExport = new ExportGIFClass();
            }
            else if (ExportType == "EMF")
            {
                docExport = new ExportEMFClass();
            }
            else if (ExportType == "JPEG")
            {
                docExport = new ExportJPEGClass();
            }
            else
            {
                MessageBox.Show("Unsupported export type " + ExportType + ", defaulting to EMF.");
                ExportType = "EMF";
                docExport = new ExportEMFClass();
            }

            docExport.ExportFileName = outputFileName;
            docExport.Resolution = 96;
            tagRECT exportRect = new tagRECT();
            exportRect.right = 1000;
            exportRect.bottom = 1000;

            IEnvelope2 pixelBounds = (IEnvelope2)new Envelope();
            pixelBounds.PutCoords(0, 0, exportRect.right, exportRect.bottom);



            docExport.PixelBounds = pixelBounds;
            int hdc = docExport.StartExporting();
            outDoc.ActiveView.Output(hdc, 96, exportRect, null, null);
            //   outDoc.ActiveView.Output(hdc, 96, exportRect, zoomOutExtent, null);

            docExport.FinishExporting();
            docExport.Cleanup();
        }



        private void ExportActiveViewParameterized(long iOutputResolution, long lResampleRatio, string ExportType, string sOutputName, Boolean bClipToGraphicsExtent, IMapDocument outDoc)
        {

            /* EXPORT PARAMETER: (iOutputResolution) the resolution requested.
             * EXPORT PARAMETER: (lResampleRatio) Output Image Quality of the export.  The value here will only be used if the export
             * object is a format that allows setting of Output Image Quality, i.e. a vector exporter.
             * The value assigned to ResampleRatio should be in the range 1 to 5.
             * 1 corresponds to "Best", 5 corresponds to "Fast"
             * EXPORT PARAMETER: (ExportType) a string which contains the export type to create.
             * EXPORT PARAMETER: (sOutputDir) a string which contains the directory to output to.
             * EXPORT PARAMETER: (bClipToGraphicsExtent) Assign True or False to determine if export image will be clipped to the graphic 
             * extent of layout elements.  This value is ignored for data view exports
             */

            /* Exports the Active View of the document to selected output format. */

            // using predefined static member
            IActiveView docActiveView = outDoc.ActiveView;
            IExport docExport;
            IPrintAndExport docPrintExport;
            IOutputRasterSettings RasterSettings;
            string sNameRoot;
            bool bReenable = false;

            if (GetFontSmoothing())
            {
                /* font smoothing is on, disable it and set the flag to reenable it later. */
                bReenable = true;
                DisableFontSmoothing();
                if (GetFontSmoothing())
                {
                    //font smoothing is NOT successfully disabled, error out.
                    return;
                }
                //else font smoothing was successfully disabled.
            }

            // The Export*Class() type initializes a new export class of the desired type.
            if (ExportType == "PDF")
            {
                docExport = new ExportPDFClass();
            }
            else if (ExportType == "EPS")
            {
                docExport = new ExportPSClass();
            }
            else if (ExportType == "AI")
            {
                docExport = new ExportAIClass();
            }
            else if (ExportType == "BMP")
            {

                docExport = new ExportBMPClass();
            }
            else if (ExportType == "TIFF")
            {
                docExport = new ExportTIFFClass();
            }
            else if (ExportType == "SVG")
            {
                docExport = new ExportSVGClass();
            }
            else if (ExportType == "PNG")
            {
                docExport = new ExportPNGClass();
            }
            else if (ExportType == "GIF")
            {
                docExport = new ExportGIFClass();
            }
            else if (ExportType == "EMF")
            {
                docExport = new ExportEMFClass();
            }
            else if (ExportType == "JPEG")
            {
                docExport = new ExportJPEGClass();
            }
            else
            {
                MessageBox.Show("Unsupported export type " + ExportType + ", defaulting to EMF.");
                ExportType = "EMF";
                docExport = new ExportEMFClass();
            }

            docPrintExport = new PrintAndExportClass();

            //set the name root for the export
            sNameRoot = "ExportActiveViewSampleOutput";

            //set the export filename (which is the nameroot + the appropriate file extension)
            docExport.ExportFileName = sOutputName; // + sNameRoot + "." + docExport.Filter.Split('.')[1].Split('|')[0].Split(')')[0];

            //Output Image Quality of the export.  The value here will only be used if the export
            // object is a format that allows setting of Output Image Quality, i.e. a vector exporter.
            // The value assigned to ResampleRatio should be in the range 1 to 5.
            // 1 corresponds to "Best", 5 corresponds to "Fast"

            // check if export is vector or raster
            if (docExport is IOutputRasterSettings)
            {
                // for vector formats, assign the desired ResampleRatio to control drawing of raster layers at export time   
                RasterSettings = (IOutputRasterSettings)docExport;
                RasterSettings.ResampleRatio = (int)lResampleRatio;

                // NOTE: for raster formats output quality of the DISPLAY is set to 1 for image export 
                // formats by default which is what should be used
            }

            docPrintExport.Export(docActiveView, docExport, iOutputResolution, bClipToGraphicsExtent, null);

            //   MessageBox.Show("Finished exporting " + sOutputDir + sNameRoot + "." + docExport.Filter.Split('.')[1].Split('|')[0].Split(')')[0] + ".", "Export Active View Sample");

            if (bReenable)
            {
                /* reenable font smoothing if we disabled it before */
                EnableFontSmoothing();
                bReenable = false;
                if (!GetFontSmoothing())
                {
                    //error: cannot reenable font smoothing.
                    MessageBox.Show("Unable to reenable Font Smoothing", "Font Smoothing error");
                }
            }
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SystemParametersInfo(uint uiAction, uint uiParam, ref int pvParam, uint fWinIni);
        const uint SPI_GETFONTSMOOTHING = 74;
        const uint SPI_SETFONTSMOOTHING = 75;
        const uint SPIF_UPDATEINIFILE = 0x1;
        private void DisableFontSmoothing()
        {
            bool iResult;
            int pv = 0;

            /* call to systemparametersinfo to set the font smoothing value */
            iResult = SystemParametersInfo(SPI_SETFONTSMOOTHING, 0, ref pv, SPIF_UPDATEINIFILE);
        }

        private void EnableFontSmoothing()
        {
            bool iResult;
            int pv = 0;

            /* call to systemparametersinfo to set the font smoothing value */
            iResult = SystemParametersInfo(SPI_SETFONTSMOOTHING, 1, ref pv, SPIF_UPDATEINIFILE);

        }

        private Boolean GetFontSmoothing()
        {
            bool iResult;
            int pv = 0;

            /* call to systemparametersinfo to get the font smoothing value */
            iResult = SystemParametersInfo(SPI_GETFONTSMOOTHING, 0, ref pv, 0);

            if (pv > 0)
            {
                //pv > 0 means font smoothing is ON.
                return true;
            }
            else
            {
                //pv == 0 means font smoothing is OFF.
                return false;
            }

        }
    }
}
