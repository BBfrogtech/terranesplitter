using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geoprocessing;
using ESRI.ArcGIS.Geodatabase;

namespace TerraneExtractor
{
    [Guid("25311f65-c048-404b-890b-863c3165d867")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("TerraneExtractor.TerranesToolBoxFactory")]
    public class TerranesToolBoxFactory : ESRI.ArcGIS.Geoprocessing.IGPFunctionFactory
    {
        public string Alias
        {
            get { return "TerranesToolBoxFactory"; }
        }

        public ESRI.ArcGIS.esriSystem.UID CLSID
        {
            get
            {
                UID pUID = new UID();
                pUID.Value = "TerraneExtractor.TerranesToolBoxFactory";
                return pUID;
            }
        }

        public ESRI.ArcGIS.Geoprocessing.IGPFunction GetFunction(string Name)
        {
            IGPFunction pGPFunction = new TerraneExtractor();
            return pGPFunction;

        }

        public  ESRI.ArcGIS.Geoprocessing.IEnumGPEnvironment GetFunctionEnvironments()
        {
            return null;
        }

        public ESRI.ArcGIS.Geodatabase.IGPName GetFunctionName(string Name)
        {
            IGPFunctionName pFunctionName = (IGPFunctionName)new GPFunctionName();
            IGPName pGPNameEdit = (IGPName)pFunctionName;
            pGPNameEdit.Category = "Terranes";
            pGPNameEdit.Description = "Terrane Extractor";
            pGPNameEdit.DisplayName = "Terrane Extractor";
            pGPNameEdit.Name = "TerraneExtractor";
            pGPNameEdit.Factory = this;
            return pGPNameEdit;
        }

        public IEnumGPName GetFunctionNames()
        {
            IGPFunctionName pFunctionName;
            IGPName pGPNameEdit;
            IArray pArray = (IArray)new EnumGPName();

            pFunctionName = (IGPFunctionName)new GPFunctionName();
            pGPNameEdit = (IGPName)pFunctionName;
            pGPNameEdit.Category = "Terranes";
            pGPNameEdit.Description = "Terrane Extractor";
            pGPNameEdit.DisplayName = "Terrane Extractor";
            pGPNameEdit.Name = "TerraneExtractor";
            pGPNameEdit.Factory = this;

            pArray.Add(pGPNameEdit);
            return (ESRI.ArcGIS.Geodatabase.IEnumGPName)pArray;
        }

        public string Name
        {
            get { return "TerranesToolBoxFactory"; }
        }
    }
}
